drop table if exists tag;

CREATE TABLE tag(
    id_tag int AUTO_INCREMENT,
    tag varchar(100),
    PRIMARY KEY (id_tag)
);

drop table if EXISTS tag_image;

CREATE TABLE tag_image(
    tag int NOT null,
    image int NOT null,
    PRIMARY KEY (tag, image),
    FOREIGN KEY (tag) REFERENCES tag(id_tag),
    FOREIGN KEY (image) REFERENCES image(id_image)
    ON DELETE CASCADE
);