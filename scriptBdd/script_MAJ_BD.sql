DROP TABLE IF EXISTS tag;

CREATE TABLE tag(
    id_tag int AUTO_INCREMENT,
    image int NOT null,
    tag varchar(255),
    PRIMARY KEY (id_tag),
    FOREIGN KEY (image) REFERENCES image(id_image)
    ON DELETE CASCADE
);


alter table image add column prix float;

alter table membre add column mot_de_passe varchar(255);