drop table if EXISTS commentaire;
drop table if exists image;
drop table if EXISTS membre;


create table image(
	id_image int AUTO_INCREMENT,
    nom varchar(24),
    image varchar(255),
    description varchar(255),
    PRIMARY KEY (id_image)
);

create table membre(
    id_membre int AUTO_INCREMENT,
    nom_utilisateur varchar(24),
    courriel varchar(500),
    PRIMARY KEY (id_membre)
);

CREATE TABLE commentaire(
    id_commentaire int AUTO_INCREMENT,
    image int,
    contenu varchar(255),
    PRIMARY KEY (id_commentaire),
    FOREIGN KEY (image) REFERENCES image(id_image)
    on DELETE CASCADE
)
