INSERT INTO image (nom, image, description) VALUES ("image1", "image/image1.jpg", "c'est la premi�re image de la BD");
INSERT INTO image (nom, image, description) VALUES ("image2", "image/image2.png", "une autre image");
INSERT INTO image (nom, image, description) VALUES ("image3", "image/image3.gif", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras leo nibh, rhoncus non mi quis, cursus mattis dolor. Nam nec erat sit amet turpis molestie convallis sed nec ipsum. Aliquam a tincidunt magna, vitae facilisis ante. Curabitur elementum sed.");
INSERT INTO image (nom, image, description) VALUES ("image4", "image/image4.jpeg", "et une quatri�me pour la posterit�");

INSERT INTO membre (courriel, nom_utilisateur) VALUES ("user1@mailnet.com", "user1");
INSERT INTO membre (courriel, nom_utilisateur) VALUES ("pasAdmin@mailnet.com", "pasAdmin");
INSERT INTO membre (courriel, nom_utilisateur) VALUES ("moi@gmail.com", "moi");
INSERT INTO membre (courriel, nom_utilisateur) VALUES ("autre@mailnet.com", "autre");

INSERT INTO commentaire (image, contenu) VALUES ("1", "elle est bien la premiere image de la BD");
INSERT INTO commentaire (image, contenu) VALUES ("1", "look at me");
INSERT INTO commentaire (image, contenu) VALUES ("1", "test 3 pour image 1");

INSERT INTO commentaire (image, contenu) VALUES ("2", "elle est bien l'autre image de la BD");
INSERT INTO commentaire (image, contenu) VALUES ("2", "c'est une autre image");

INSERT INTO commentaire (image, contenu) VALUES ("3", "oui, lorem ipsum");
INSERT INTO commentaire (image, contenu) VALUES ("3", "test 3 image 3");

INSERT INTO commentaire (image, contenu) VALUES ("4", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras leo nibh, rhoncus non mi quis, cursus mattis dolor. Nam nec erat sit amet turpis molestie convallis sed nec ipsum. Aliquam a tincidunt magna, vitae facilisis ante. Curabitur elementum sed.");