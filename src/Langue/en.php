<?php

  msgid""
  msgstr""

  "Project-ID-Version: p2pimage 1.0.1\n"
  "Language: en\n"
  "MIME-Version: 1.0\n"
  "Content-Type: text/plain; charset=UTF-8\n"

  msgid "A propos"
  msgstr "About us"

  msgid "p2pImage.com est un site de vente d'images entre particuliers."
  msgstr "p2pImage.com is a peer to peer pictures market online"

  msgid "Photographes amateurs, dessinateurs, peintres ou experts photoshop, vous pouvez vendre vos oeuvres au monde entier et trouver de nouvelles sources d'inspiration ou de la matière à revisiter."
  msgstr "Amateur photographers, designers, painters or photoshop experts, you can sell your works to the world and find new sources of inspiration or material to revisit."

  msgid "Acceuil"
  msgstr "Home"

  msgid "Profil"
  msgstr "Profile"

  msgid "Mettre une image en vente"
  msgstr "Sell a picture"

  msgid "Connexion"
  msgstr "Connection"

  msgid "Inscription"
  msgstr "Sign up"

  msgid "Achat d'une image"
  msgstr "Buy a picture"

  msgid "Premiere étape"
  msgstr "First step"

  msgid "Acheter"
  msgstr "Buy"

  msgid "Continuer de magasiner"
  msgstr "Continue shopping"

  msgid "Retour"
  msgstr "Return"

  msgid "Payer"
  msgstr "Pay"

  msgid "Retourner à la seconde étape"
  msgstr "Return to the second step"

  msgid "Terminer le payment"
  msgstr "Finish the payment "

  msgid "Retourner à la page"
  msgstr "Return to page "

  msgid "Se connecter en tant que membre"
  msgstr "Connect as member"

  msgid "Mot de passe"
  msgstr "Password"

  msgid "S'identifier"
  msgstr "Log in"

  msgid "Vous avez oublié votre mot de passe ?"
  msgstr "Password forgotten?"

  msgid "Detail image"
  msgstr "Picture detail"

  msgid "commente"
  msgstr "comment"

  msgid "soumettre"
  msgstr "submit"

  msgid "supprimer"
  msgstr "delete"

  msgid "Nom d'utilisateur"
  msgstr "User name"

  msgid "Courriel"
  msgstr "e-mail"

  msgid "Confirmation"
  msgstr "Confirmation"

  msgid "Valider"
  msgstr "Validate"

  msgid "Liste des images"
  msgstr "Pictures list"

  msgid "Voici la liste des images"
  msgstr "Here is the pictures list"

  msgid "Liste des utilisateurs"
  msgstr "User list"

  msgid "Panneau d'administration"
  msgstr "Managment panel"

  msgid "Bienvenu sur le paneau d'administration"
  msgstr "Welcome to the managment panel"

  msgid "Affichage du profil"
  msgstr "Profile display"

  msgid "Informations"
  msgstr "Informations"

  msgid "Ajouter la personne"
  msgstr "Add person"

  msgid "Modifier la personne"
  msgstr "Modify person"

  msgid "Supprimer la personne"
  msgstr "Delete person"

  msgid " images supplémentaires"
  msgstr "more pictures"

  msgid "Voir la gallerie complète"
  msgstr "See the complete gallery"

  msgid "Chercher une image"
  msgstr "Find a picture"

  msgid "Prix maximum:"
  msgstr "Maximum price"

  msgid "Rechercher l'image"
  msgstr "Search picture"

  msgid "Suppression image"
  msgstr "Delete picture"

  msgid "confirmer la suppression"
  msgstr "Confirm deletion"

  msgid "Annuler la suppression"
  msgstr "Cancel deletion"

  msgid "Vendre une image"
  msgstr "Sell a picture"

  msgid "Titre de l'image"
  msgstr "Picture title"

  msgid "Prix :"
  msgstr "Price :"

  msgid "Glissez vos images ici !"
  msgstr "Drop your picture here"

  msgid "Tags :"
  msgstr "Tags :"

  msgid "tag1, tag2, ..."
  msgstr "tag1, tag2, ..."

  msgid "Vendre l'image"
  msgstr "Sell picture"

  msgid "Seconde étape"
  msgstr "Second step"

  msgid "Erreur à la première étape"
  msgstr "Error in the first step"

  msgid "Premiere étape"
  msgstr "First step"

  msgid "Troisième étape"
  msgstr "Third step"

  msgid "Erreur à la seconde étape"
  msgstr "Error in the second step"

  msgid "Achat réussi"
  msgstr "Purchase success"

  msgid "Erreur lors de l'achat"
  msgstr "Purchase error"

  msgid "Erreur à la troisième étape"
  msgstr "Error in the third step"

  msgid "Ajouter un utilisateur"
  msgstr "Add user"

  msgid "Modifier"
  msgstr "Modify"

  msgid "Supprimer"
  msgstr "Delete"

  msgid "Gerer les utilisateurs"
  msgstr "Manage users"

  msgid "Gerer les images"
  msgstr "Manage pictures"

  msgid "Les détails d'une personne"
  msgstr "Details of a person"

  msgid "Ajouter une personne"
  msgstr "Add a person"

  msgid "Modifier une personne"
  msgstr "Modify a person"

  msgid "Supprimer une personne"
  msgstr "Delete a person"

  msgid "Suppression de personne réussie"
  msgstr "Delete of the person succesful"

  msgid "Erreur de suppression de personne"
  msgstr "Error deleting the person"

  msgid "Ajout de personne réussie"
  msgstr "Add of a person succesful"

  msgid "Erreur d'ajout de personne"
  msgstr "Error adding a person"

  msgid "Modification de personne réussie"
  msgstr "Modification of a person succesful"

  msgid "Erreur de modification de personne"
  msgstr "Error modifying a person"

  msgid "Utilisateur non trouvée"
  msgstr "User not found"

  msgid "mise en vente réussie"
  msgstr "sale successful"

  msgid "Erreur lors de la vente"
  msgstr "Error during sale"

  msgid "Vendre une image"
  msgstr "Sell a picture"

  msgid "Description"
  msgstr "Description"
 ?>
