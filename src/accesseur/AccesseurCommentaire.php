<?php

require_once("../configuration/configuration.dev.php");
require_once("../modele/Commentaire.php");


class AccesseurCommentaire {

    private const SUBTITUT_ID_COMMENTAIRE = ":" .
                   Commentaire::ID_COMMENTAIRE;

    private const SUBTITUT_CONTENU = ":" .
                   Commentaire::CONTENU;

    private const SUBTITUT_IMAGE_COMMENTAIRE = ":idImage";


    private static $RECUPERER_COMMENTAIRE_SQL =
        " SELECT " .
        Commentaire::ID_COMMENTAIRE . "," .
        Commentaire::CONTENU . " " .
        " FROM commentaire " .
        " WHERE " .
        Commentaire::ID_COMMENTAIRE . " = " .
        self::SUBTITUT_ID_COMMENTAIRE;

    private static $RECUPERER_LISTE_COMMENTAIRE_SQL =
        "SELECT " .
        Commentaire::ID_COMMENTAIRE . "," .
        Commentaire::CONTENU . " " .
        "FROM commentaire";

    private static $RECUPERER_LISTE_COMMENTAIRE_PAR_IMAGE_SQL =
        "SELECT " .
        Commentaire::ID_COMMENTAIRE . "," .
        Commentaire::CONTENU . " " .
        " FROM commentaire" .
        " WHERE image =" .
        self::SUBTITUT_IMAGE_COMMENTAIRE;


    private static $SUPPRIMER_COMMENTAIRE_SQL =
        "DELETE FROM commentaire " .
        " WHERE " .
        Commentaire::ID_COMMENTAIRE . "=" .
        self::SUBTITUT_ID_COMMENTAIRE;

    private static $MODIFIER_COMMENTAIRE_SQL =
        "UPDATE commentaire " .
        " SET " .
        Commentaire::CONTENU . "=" . self::SUBTITUT_CONTENU . " " .
        " WHERE " .
        Commentaire::ID_COMMENTAIRE . " = " .
        self::SUBTITUT_ID_COMMENTAIRE;

    private static $AJOUTER_COMMENTAIRE_SQL =
        "INSERT INTO commentaire " .
        "( " .
        Commentaire::CONTENU . ",image ) VALUES (" . self::SUBTITUT_CONTENU . ",".self::SUBTITUT_IMAGE_COMMENTAIRE.")";

    private static $connexion = null;

    function __construct(){

        if(!self::$connexion){
             self::$connexion = BaseDeDonnee::getConnexion();
        }
    }

    public function recupererCommentaire($commentaire){

        $id_commentaire =  $commentaire->getId_commentaire();

        if(!$commentaire->isValide(Commentaire::ID_COMMENTAIRE))
            return false;

        $requete = self::$connexion->prepare(self::$RECUPERER_COMMENTAIRE_SQL);

        $requete->bindValue(
            self::SUBTITUT_ID_COMMENTAIRE,
            $id_commentaire,
            PDO::PARAM_INT);

        $requete->execute();

        return new Commentaire($requete->fetchObject());

    }

    public function recupererListeCommentaire(){

        $listeCommentaire = [];

        $requete =
            self::$connexion->prepare(self::$RECUPERER_LISTE_COMMENTAIRE_SQL);

        $requete->execute();

        $listeEnregistrement = $requete->fetchAll(PDO::FETCH_OBJ);

        foreach($listeEnregistrement as $enregistrement) {

            $listeCommentaire[] = new Commentaire($enregistrement);

        }

        return $listeCommentaire;

    }


    public function recupererListeCommentaireParImage($id_image){

        $listeCommentaire = [];
        $requete =
            self::$connexion->prepare(self::$RECUPERER_LISTE_COMMENTAIRE_PAR_IMAGE_SQL);

         $requete->bindValue(
            self::SUBTITUT_IMAGE_COMMENTAIRE,
            $id_image,
            PDO::PARAM_INT);

        $requete->execute();

        $listeEnregistrement = $requete->fetchAll(PDO::FETCH_OBJ);

        foreach($listeEnregistrement as $enregistrement) {



            $commentaire = new Commentaire($enregistrement);
            $listeCommentaire[] = $commentaire;

        }

        return $listeCommentaire;

    }

    public function supprimerEntiteAffaire($comentaire){

        $id_commentaire =
            $commentaire->getId_commentaire();

        if(!$comentaire->isValide(Commentaire::ID_COMMENTAIRE))
            return false;

        $requete = self::$connexion->prepare(self::$SUPPRIMER_COMMENTAIRE_SQL);

        $requete->bindValue(
            self::SUBTITUT_ID_COMMENTAIRE,
            $id_commentaire,
            PDO::PARAM_INT);

        $requete->execute();

        return $requete->rowCount() > 0;

    }

    public function modifierEntiteAffaire($commentaire){

        $id_commentaire =  $commentaire->getId_commentaire();

        if(!$commentaire->isValide()) return false;

        $requete = self::$connexion->prepare(self::$MODIFIER_COMMENTAIRE_SQL);

        $requete->bindValue(
            self::SUBTITUT_ID_COMMENTAIRE,
            $id_commentaire,
            PDO::PARAM_INT);

        $requete->bindValue(
            self::SUBTITUT_CONTENU,
            $commentaire->getContenu(),
            PDO::PARAM_STR);


        $requete->execute();

        return $requete->rowCount() > 0;

    }

    public function ajouterEntiteAffaire($commentaire,$idImage){

        $valide = $commentaire->isValide();

        if(!$valide) return false;

        $requete = self::$connexion->prepare(self::$AJOUTER_COMMENTAIRE_SQL);


        $requete->bindValue(
            self::SUBTITUT_CONTENU,
            $commentaire->getContenu(),
            PDO::PARAM_STR);

        $requete->bindValue(
            self::SUBTITUT_IMAGE_COMMENTAIRE,
            $idImage,
            PDO::PARAM_INT);


        $requete->execute();

        if($requete->rowCount() > 0){

            $commentaire->setId_commentaire(self::$connexion->lastInsertId());
            return $commentaire;

        }

        return false;

    }

}
