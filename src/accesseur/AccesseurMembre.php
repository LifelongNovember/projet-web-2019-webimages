<?php
require_once("../configuration/configuration.dev.php");
require_once("../modele/Membre.php");

class AccesseurMembre
{
  private const SUBTITUT_ID_MEMBRE = ":" . Membre::ID_MEMBRE;
  private const SUBTITUT_NOM_UTILISATEUR = ":" . Membre::NOM_UTILISATEUR;
  private const SUBTITUT_COURRIEL = ":" . Membre::COURRIEL;
  private const SUBTITUT_MOT_DE_PASSE = ":" . Membre::MOT_DE_PASSE;
  private const SUBTITUT_EST_ADMIN = ":" . Membre::EST_ADMIN;

  private static $RECUPERER_MEMBRE_SQL =
                          "SELECT " .
                          Membre::ID_MEMBRE . "," .
                          Membre::NOM_UTILISATEUR  . "," .
                          Membre::COURRIEL . "," .
                          Membre::MOT_DE_PASSE . ", " .
                          Membre::EST_ADMIN . " " .
                          "FROM membre " .
                          "WHERE " .
                          Membre::ID_MEMBRE . "=" .
                          self::SUBTITUT_ID_MEMBRE;

private static $RECUPERER_MEMBRE_PAR_NOM_SQL =
                          "SELECT " .
                          Membre::ID_MEMBRE . " , " .
                          Membre::NOM_UTILISATEUR  . " , " .
                          Membre::COURRIEL . " , " .
                          Membre::MOT_DE_PASSE . " , " .
                          Membre::EST_ADMIN . "  " .
                          "FROM membre WHERE " .
                          Membre::NOM_UTILISATEUR . " = " .
                          self::SUBTITUT_NOM_UTILISATEUR .
                          " AND ". Membre::MOT_DE_PASSE .
                          " = " . self::SUBTITUT_MOT_DE_PASSE ;

  private static $RECUPERER_LISTE_MEMBRE_SQL =
                            "SELECT " .
                            Membre::ID_MEMBRE . "," .
                            Membre::NOM_UTILISATEUR . "," .
                            Membre::COURRIEL . "," .
                            Membre::MOT_DE_PASSE . ", " .
                            Membre::EST_ADMIN . " " .
                            "FROM membre";

  private static $SUPPRIMER_MEMBRE_SQL =
                            "DELETE FROM membre " .
                            "WHERE " .
                            Membre::ID_MEMBRE . " = " .
                            self::SUBTITUT_ID_MEMBRE;

  private static $MODIFIER_MEMBRE_SQL =
                            "UPDATE membre " .
                            "SET " .
                            Membre::NOM_UTILISATEUR  . "=" . self::SUBTITUT_NOM_UTILISATEUR ." , " .
                            Membre::COURRIEL . "=" . self::SUBTITUT_COURRIEL . " , " .
                            Membre::MOT_DE_PASSE . "=" . self::SUBTITUT_MOT_DE_PASSE . ", " .
                            Membre::EST_ADMIN . "=" . self::SUBTITUT_EST_ADMIN . " " .
                            "WHERE " .
                            Membre::ID_MEMBRE . "=" .
                            self::SUBTITUT_ID_MEMBRE;

  private static $AJOUTER_MEMBRE_SQL =
                            "INSERT INTO membre " .
                            "SET " .
                            Membre::NOM_UTILISATEUR . " = " . self::SUBTITUT_NOM_UTILISATEUR . " , " .
                            Membre::COURRIEL . " = " . self::SUBTITUT_COURRIEL . " , " .
                            Membre::MOT_DE_PASSE . " = " . self::SUBTITUT_MOT_DE_PASSE;


    private static $connexion = null;
    Function __construct()
    {

      if(!self::$connexion)self::$connexion =  BaseDeDonnee::getConnexion();

      }


    public function recupererMembre($membre)
      {

         $requete = self::$connexion->prepare(self::$RECUPERER_MEMBRE_SQL);
         //var_dump($membre);
         $requete->bindValue(
             self::SUBTITUT_ID_MEMBRE,
             $membre->getId_membre(),
             PDO::PARAM_INT);

         $requete->execute();

         return new Membre($requete->fetchObject());

     }

     public function recupererListeMembre()
     {

          $listeMembre = [];

          $requete = self::$connexion->prepare(self::$RECUPERER_LISTE_MEMBRE_SQL);

          $requete->execute();

          $listeEnregistrement = $requete->fetchAll(PDO::FETCH_OBJ);

          foreach($listeEnregistrement as $enregistrement)
          {

              $listeMembre[] = new Membre($enregistrement);

          }

          return $listeMembre;

        }
        public function supprimerMembre($membre){

          //if(empty($id_membre)) return false;

          $requete = self::$connexion->prepare(self::$SUPPRIMER_MEMBRE_SQL);

          $requete->bindValue(
              self::SUBTITUT_ID_MEMBRE,
              $membre->getId_membre(),
              PDO::PARAM_INT);

          $requete->execute();
          //var_dump($requete->rowCount());
          return $requete->rowCount() > 0;

      }

      public function modifierMembre($membre)
      {

         //$id_membre = $membre->getId_membre();
         //if(empty($id_membre)) return false;

         //if(!$membre->isValide()) return false;

         $requete = self::$connexion->prepare(self::$MODIFIER_MEMBRE_SQL);

         $requete->bindValue(
             self::SUBTITUT_ID_MEMBRE,
             $membre->getId_membre(),
             PDO::PARAM_INT);

         $requete->bindValue(
             self::SUBTITUT_NOM_UTILISATEUR ,
             $membre->getNom_utilisateur(),
             PDO::PARAM_STR);

         $requete->bindValue(
             self::SUBTITUT_COURRIEL,
             $membre->getCourriel(),
             PDO::PARAM_STR);

         $requete->bindValue(
             self::SUBTITUT_MOT_DE_PASSE,
             $membre->getMotDePasse(),
             PDO::PARAM_STR);

          $requete->bindValue(
              self::SUBTITUT_EST_ADMIN,
              $membre->getEstAdminInt(),
              PDO::PARAM_STR);

         $requete->execute();

         return $requete->rowCount() > 0;

     }
     public function ajouterMembre($membre)
     {

      //  if(!$membre->isValide()) return false;

        $requete = self::$connexion->prepare(self::$AJOUTER_MEMBRE_SQL);

        $requete->bindValue(
            self::SUBTITUT_NOM_UTILISATEUR,
            $membre->getNom_utilisateur(),
            PDO::PARAM_STR);

        $requete->bindValue(
            self::SUBTITUT_COURRIEL,
            $membre->getCourriel(),
            PDO::PARAM_STR);

        $requete->bindValue(
            self::SUBTITUT_MOT_DE_PASSE,
            $membre->getMotDePasse(),
            PDO::PARAM_STR);

        $requete->execute();

        if($requete->rowCount() > 0)
        {

            $membre->setId_membre(self::$connexion->lastInsertId());
            return $membre;

      }

      return false;

  }
  public function recupererMembreParNom($membre)
    {

       $requete = self::$connexion->prepare(self::$RECUPERER_MEMBRE_PAR_NOM_SQL);

       $requete->bindValue(
           self::SUBTITUT_NOM_UTILISATEUR,
           $membre->getNom_utilisateur(),
           PDO::PARAM_STR);

       $requete->bindValue(
           self::SUBTITUT_MOT_DE_PASSE,
           $membre->getMotDePasse(),
           PDO::PARAM_STR);

       $requete->execute();
       //print_r($requete->errorInfo());
       return new Membre($requete->fetchObject());

   }


}



 ?>
