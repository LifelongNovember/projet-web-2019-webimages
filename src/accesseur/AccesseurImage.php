<?php
require_once("../configuration/configuration.dev.php");
require_once('../modele/Image.php');
class AcesseurImage{

  private const SUBSTITUE_ID_IMAGE = ':'.Image::ID_IMAGE;
  private const SUBSTITUE_IMAGE = ':'.Image::IMAGE;
  private const SUBSTITUE_NOM = ':'.Image::NOM;
  private const SUBSTITUE_PRIX = ':'.Image::PRIX;
  private const SUBSTITUE_DESCRIPTION = ':'.Image::DESCRIPTION;
  private const SUBSTITUE_RECHERCHE = ':RECHERCHE';
  private static $RECUPERER_LISTE_IMAGE_SQL =
    "SELECT ".Image::ID_IMAGE.','.Image::IMAGE.','.Image::NOM.','.Image::DESCRIPTION.','.Image::PRIX.
    " FROM image";

  private static $RECUPERER_IMAGE_SQL =
    "SELECT ".Image::ID_IMAGE.','.Image::IMAGE.','.Image::NOM.','.Image::DESCRIPTION.','.Image::PRIX.
    " FROM image WHERE ".Image::ID_IMAGE.'='.self::SUBSTITUE_ID_IMAGE;

  private static $SUPPRIMER_IMAGE_SQL =
    "DELETE FROM image WHERE ".Image::ID_IMAGE.' = '.self::SUBSTITUE_ID_IMAGE;

  private static $MODIFIER_IMAGE_SQL =
    "UPDATE image SET ".Image::IMAGE.'='.self::SUBSTITUE_IMAGE.','.Image::NOM.'='.
    self::SUBSTITUE_NOM.','.Image::DESCRIPTION.'='.self::SUBSTITUE_DESCRIPTION." WHERE ".
    Image::ID_IMAGE.'='.self::SUBSTITUE_ID_IMAGE.','.Image::PRIX.'='.self::SUBSTITUE_PRIX;

  private static $AJOUTER_IMAGE_SQL =
    "INSERT image SET ".Image::IMAGE.'='.self::SUBSTITUE_IMAGE.','.Image::NOM.'='.
    self::SUBSTITUE_NOM.','.Image::DESCRIPTION.'='.self::SUBSTITUE_DESCRIPTION.','.
    Image::PRIX.'='.self::SUBSTITUE_PRIX;


  private static $RECHERCHER_SUGGESTIONS_IMAGE_SQL =
      "SELECT ".Image::ID_IMAGE.','.Image::IMAGE.','.Image::NOM.','.Image::DESCRIPTION.','.Image::PRIX.
      " FROM image WHERE ".Image::NOM.' '."LIKE CONCAT('%', ".self::SUBSTITUE_RECHERCHE.", '%')";

    private static $connextion = null;

  function __construct(){
    if(self::$connextion  == null) {
      $basededonnees = new BaseDeDonnee();
      self::$connextion = $basededonnees->getConnexion();
    }
  }

  public function ajouterImage($image){
    $erreurs = $image->getListeErreurActive();
    if(!empty($erreurs)) return false;
    $requete = self::$connextion->prepare(self::$AJOUTER_IMAGE_SQL);

    $requete->bindParam(self::SUBSTITUE_NOM, $image->getNom(), PDO::PARAM_STR);
    $requete->bindParam(self::SUBSTITUE_IMAGE, $image->getImage(), PDO::PARAM_STR);
    $requete->bindParam(self::SUBSTITUE_DESCRIPTION, $image->getDescription(), PDO::PARAM_STR);
    $requete->bindParam(self::SUBSTITUE_PRIX, $image->getPrix(), PDO::PARAM_STR);

    $requete->execute();

    if($requete->rowCount() > 0){
      $image->setId_image(self::$connextion->lastInsertId());
      return $image;
    }
    return false;
  }

  public function modifierImage($image){
    $erreurs = $image->getListeErreurActive();
    if(!empty($erreurs)) return false;

    $requete = self::$connextion->prepare(self::$MODIFIER_IMAGE_SQL);

    $requete->bindParam(self::SUBSTITUE_NOM, $image->getNom(), PDO::PARAM_STR);
    $requete->bindParam(self::SUBSTITUE_IMAGE, $image->getImage(), PDO::PARAM_STR);
    $requete->bindParam(self::SUBSTITUE_DESCRIPTION, $image->getDescription(), PDO::PARAM_STR);
    $requete->bindParam(self::SUBSTITUE_PRIX, $image->getPrix(), PDO::PARAM_STR);

    $requete->execute();

    if($requete->rowCount() > 0){
      $image->setId_image(self::$connextion->lastInsertId());
      return $image;
    }
    return false;
  }

  public function suprimerImage($image){
    $erreurs = $image->getListeErreurActive();
    if(!empty($erreurs)) return false;

    $id_image = $image->getId_image();
    if(empty($id_image)) return false;

    $requete = self::$connextion->prepare(self::$SUPPRIMER_IMAGE_SQL);

    $requete->bindValue( self::SUBSTITUE_ID_IMAGE, $id_image, PDO::PARAM_INT);
    $requete->execute();

    return $requete->rowCount() > 0;
  }

  public function recupererImage($id_image){

    if(empty($id_image)) return false;

    $requete = self::$connextion->prepare(self::$RECUPERER_IMAGE_SQL);

    $requete->bindValue( self::SUBSTITUE_ID_IMAGE, $id_image, PDO::PARAM_INT);
    $requete->execute();

    return new Image($requete->fetchObject());
  }

  public function recupererListeImage(){
    $listeImage = [];

    $requete = self::$connextion->prepare(self::$RECUPERER_LISTE_IMAGE_SQL);
    $requete->execute();

    $listeResultat = $requete->fetchAll(PDO::FETCH_OBJ);

    foreach($listeResultat as $resultat){
      $listeImage[] = new Image($resultat);
    }

    return $listeImage;
  }

  function rechercherSuggestionsImage($recherche)
		{

		//	$RECHERCHER_SUGGESTIONS_IMAGE_SQL = "SELECT nom FROM image WHERE nom LIKE '%" . $recherche . "%'";
			//echo $RECHERCHER_SUGGESTIONS_IMAGE_SQL ;

    $requete = self::$connextion->prepare(self::$RECHERCHER_SUGGESTIONS_IMAGE_SQL);
    $requete->bindValue( self::SUBSTITUE_RECHERCHE, $recherche, PDO::PARAM_STR);
    $requete->execute();

    $suggestions = $requete->fetchAll(PDO::FETCH_OBJ);

    $listeImage = [];
    foreach($suggestions as $resultat){
      $listeImage[] = new Image($resultat);
    }

		//print_r($suggestions);
		return $listeImage;

		}
}
 ?>
