<?php
require_once("../configuration/configuration.dev.php");
require_once '../modele/Tag.php';
class AcesseurTag{

  private const SUBSTITUE_ID_TAG = ':'.Tag::ID_TAG;
  private const SUBSTITUE_NOM = ':'.Tag::NOM;

  private static $RECUPERER_LISTE_TAG_SQL =
    "SELECT ".Tag::ID_TAG.','.Tag::NOM." FROM tag";

  private static $RECUPERER_TAG_SQL =
    "SELECT ".Tag::ID_TAG.','.Tag::NOM." FROM tag WHERE ".Tag::ID_TAG.'='.self::SUBSTITUE_ID_TAG;

  private static $SUPPRIMER_TAG_SQL =
    "DELETE FROM tag WHERE ".Tag::ID_TAG.' = '.self::SUBSTITUE_ID_TAG;

  private static $MODIFIER_TAG_SQL =
    "UPDATE tag SET ".Tag::NOM.'='.self::SUBSTITUE_NOM." WHERE ".Tag::ID_TAG.'='.self::SUBSTITUE_ID_TAG;

  private static $AJOUTER_TAG_SQL =
    "INSERT tag SET ".Tag::NOM.'='.self::SUBSTITUE_NOM.;

    private static $connextion = null;

  function __construc(){
    if(!self::$connextion  == null) self::$connextion = $basededonnees.getConnexion();
  }

  public function ajouterTag($tag){
    $erreurs = $tag->getListeErreurActive();
    if(!empty($erreurs)) return false;

    $requete = $connextion->prepare(self::$AJOUTER_TAG_SQL);

    $requete->bindParam(self::$SUBSTITUE_NOM, $tag->getNom(), PDO::PARAM_STR);

    $requete->execute();

    if($requete->rowCount() > 0){
      $tag->setId_tag($connextion->lastInsertId());
      return $tag;
    }
    return false;
  }

  public function modifierTag($tag){
    $erreurs = $tag->getListeErreurActive();
    if(!empty($erreurs)) return false;

    $requete = $connextion->prepare(self::$MODIFIER_TAG_SQL);

    $requete->bindParam(self::$SUBSTITUE_NOM, $tag->getNom(), PDO::PARAM_STR);

    $requete->execute();

    if($requete->rowCount() > 0){
      $tag->setId_tag($connextion->lastInsertId());
      return $tag;
    }
    return false;
  }

  public function suprimerTag($tag){
    $erreurs = $tag->getListeErreurActive();
    if(!empty($erreurs)) return false;

    $id_tag = $tag->getTag();
    if(empty($id_tag)) return false;

    $requete = $connexion->prepare(self::$SUPPRIMER_TAG_SQL);

    $requete->bindValue( self::SUBSTITUE_ID_TAG, $id_tag, PDO::PARAM_INT);
    $requete->execute();

    return $requete->rowCount() > 0;
  }

  public function recupererTag($tag){
    $erreurs = $tag->getListeErreurActive();
    if(!empty($erreurs)) return false;

    $id_tag = $tag->getTag();
    if(empty($id_tag)) return false;

    $requete = $connexion->prepare(self::$RECUPERER_TAG_SQL);

    $requete->bindValue( self::SUBSTITUE_ID_TAG, $id_tag, PDO::PARAM_INT);
    $requete->execute();

    return new Tag($requete->fetchObject());
  }

  public function recupererListeTag(){
    $listeTag = [];

    $requete = $connexion->prepare(self::$RECUPERER_LISTE_TAG_SQL);
    $requete->execute();

    $listeResultat = $requete->fetchAll(PDO::PARAM_OBJ);

    foreach($listeResultat as $resultat){
      $listeTag[] = new Tag($resultat);
    }

    return $resultat;
  }

}
 ?>
