<?php
require_once("../configuration/configuration.dev.php");
require_once('../modele/Payment.php');
class AcesseurPayment{

  private const SUBSTITUE_ID_PAYMENT = ':'.Payment::ID_PAYMENT;
  private const SUBSTITUE_ID_IMAGE = ':'.Payment::ID_IMAGE;
  private const SUBSTITUE_ID_MEMBRE = ':'.Payment::ID_MEMBRE;
  private const SUBSTITUE_HEURE_PAYMENT = ':'.Payment::HEURE_PAYMENT;

  private static $RECUPERER_LISTE_PAYMENT_SQL =
    "SELECT ".Payment::ID_PAYMENT.','.Payment::ID_IMAGE.','.Payment::ID_MEMBRE.','.Payment::HEURE_PAYMENT.
    " FROM payment";

  private static $RECUPERER_PAYMENT_ID_MEMBRE_SQL =
    "SELECT ".Payment::ID_PAYMENT.','.Payment::ID_IMAGE.','.Payment::ID_MEMBRE.','.Payment::HEURE_PAYMENT.
    " FROM payment WHERE ".Payment::ID_MEMBRE.'='.self::SUBSTITUE_ID_MEMBRE;

  private static $RECUPERER_PAYMENT_SQL =
  "SELECT ".Payment::ID_PAYMENT.','.Payment::ID_IMAGE.','.Payment::ID_MEMBRE.','.Payment::HEURE_PAYMENT.
  " FROM payment WHERE ".Payment::ID_PAYMENT.'='.self::SUBSTITUE_ID_PAYMENT;

  private static $SUPPRIMER_PAYMENT_SQL =
    "DELETE FROM payment WHERE ".Payment::ID_PAYMENT.' = '.self::SUBSTITUE_ID_PAYMENT;

  private static $MODIFIER_PAYMENT_SQL =
    "UPDATE payment SET ".Payment::ID_IMAGE.'='.self::SUBSTITUE_ID_IMAGE.','.
    Payment::ID_MEMBRE.'='.self::SUBSTITUE_ID_MEMBRE.','.
    Payment::HEURE_PAYMENT.'='.self::SUBSTITUE_HEURE_PAYMENT." WHERE ".
    Payment::ID_PAYMENT.'='.self::SUBSTITUE_ID_PAYMENT;

  private static $AJOUTER_PAYMENT_SQL =
    "INSERT payment SET ".Payment::ID_IMAGE.'='.self::SUBSTITUE_ID_IMAGE.','.
    Payment::ID_MEMBRE.'='.self::SUBSTITUE_ID_MEMBRE;

    private static $connextion = null;

  function __construct(){
    if(self::$connextion  == null) {
      $basededonnees = new BaseDeDonnee();
      self::$connextion = $basededonnees->getConnexion();
    }
  }

  public function ajouterPayment($payment){
    $requete = self::$connextion->prepare(self::$AJOUTER_PAYMENT_SQL);

    $requete->bindParam(self::SUBSTITUE_ID_IMAGE, $payment->getId_image(), PDO::PARAM_INT);
    $requete->bindParam(self::SUBSTITUE_ID_MEMBRE, $payment->getId_membre(), PDO::PARAM_INT);

    $requete->execute();

    if($requete->rowCount() > 0){
      $payment->setId_payment(self::$connextion->lastInsertId());
      return $payment;
    }
    return false;
  }

  public function modifierPayment($payment){
    $erreurs = $payment->getListeErreurActive();
    if(!empty($erreurs)) return false;

    $requete = self::$connextion->prepare(self::$MODIFIER_PAYMENT_SQL);

    $requete->bindParam(self::SUBSTITUE_ID_IMAGE, $payment->getId_image(), PDO::PARAM_INT);
    $requete->bindParam(self::SUBSTITUE_ID_MEMBRE, $payment->getId_membre(), PDO::PARAM_INT);
    $requete->bindParam(self::SUBSTITUE_HEURE_PAYMENT, $payment->getHeure_payment(), PDO::PARAM_INT);
    $requete->bindParam(self::SUBSTITUE_ID_PAYMENT, $payment->getId_payment(), PDO::PARAM_INT);

    $requete->execute();

    if($requete->rowCount() > 0){
      $payment->setId_payment(self::$connextion->lastInsertId());
      return $payment;
    }
    return false;
  }

  public function suprimerPayment($payment){
    $erreurs = $payment->getListeErreurActive();
    if(!empty($erreurs)) return false;

    $id_payment = $payment->getId_payment();
    if(empty($id_payment)) return false;

    $requete = self::$connextion->prepare(self::$SUPPRIMER_PAYMENT_SQL);

    $requete->bindValue( self::SUBSTITUE_ID_PAYMENT, $id_payment, PDO::PARAM_INT);
    $requete->execute();

    return $requete->rowCount() > 0;
  }

  public function recupererPayment($id_payment){

    if(empty($id_payment)) return false;

    $requete = self::$connextion->prepare(self::$RECUPERER_PAYMENT_SQL);

    $requete->bindValue( self::SUBSTITUE_ID_PAYMENT, $id_payment, PDO::PARAM_INT);
    $requete->execute();

    return new Payment($requete->fetchObject());
  }

  public function recupererListePayment(){
    $listePayment = [];

    $requete = self::$connextion->prepare(self::$RECUPERER_LISTE_PAYMENT_SQL);
    $requete->execute();

    $listeResultat = $requete->fetchAll(PDO::FETCH_OBJ);

    foreach($listeResultat as $resultat){
      $listePayment[] = new Payment($resultat);
    }

    return $listePayment;
  }
  public function recupererPaymentIdMembre($id_membre){
    if(empty($id_membre)) return false;

    $requete = self::$connextion->prepare(self::$RECUPERER_PAYMENT_ID_MEMBRE_SQL);

    $requete->bindValue( self::SUBSTITUE_ID_MEMBRE, $id_membre, PDO::PARAM_INT);
    $requete->execute();
    $listePayment = [];

    $listeResultat = $requete->fetchAll(PDO::FETCH_OBJ);
    foreach($listeResultat as $resultat){
      $listePayment[] = new Payment($resultat);
    }
    return $listePayment;
  }

}
 ?>
