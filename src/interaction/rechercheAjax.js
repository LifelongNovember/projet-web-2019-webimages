(function(){

  var ajax;
  var barreRecherche;
  var conteneurSuggestion;

  function initialiser(){
    
    if(document.getElementById("barreRecherche-index")) barreRecherche = document.getElementById("barreRecherche-index");
    else if(document.getElementById("barreRecherche-recherche")) barreRecherche = document.getElementById("barreRecherche-recherche");
    else barreRecherche = document.getElementById("barreRecherche");
    
    if(document.getElementById("suggestion-index")) conteneurSuggestion = document.getElementById("suggestion-index");
    else if(document.getElementById("suggestion-recherche")) conteneurSuggestion = document.getElementById("suggestion-recherche");
    else conteneurSuggestion = document.getElementById("suggestion");
    
    barreRecherche.onkeyup = recherche;
    ajax = new Ajax();
  }

  function recherche(){
    if(barreRecherche.value != "")
      ajax.executer("GET", "https://p2pimage.com/suggestion.php", "recherche=" + barreRecherche.value, afficher);
    else
      conteneurSuggestion.innerHTML = "";
  }

  function afficher(ajax){
    conteneurSuggestion.innerHTML = ajax.responseText;
  }

  initialiser();
})();
