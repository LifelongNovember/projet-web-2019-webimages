<?php

class Commentaire
{

    public  const ID_COMMENTAIRE = "id_commentaire";
    public  const CONTENU = "contenu";

    private static $LISTE_MESSAGE_ERREUR = [];
    private static $LISTE_INFORMATION_CHAMP = [];



    private static function getListeMessageErreur(){

        if(empty(self::$LISTE_MESSAGE_ERREUR)){

            self::$LISTE_MESSAGE_ERREUR =
            [

                "commentaire-vide" =>
                    "Le commentaire est vide",
                "commentaire-trop-long" =>
                    "Le commentaire est trop long , la longueur maximal autoriser est de 200 caractere ",
                "id_commentaire-invalide" =>
                    true];

        }

        return self::$LISTE_MESSAGE_ERREUR;

    }



    private $listeMessageErreurActif = [];

    private $id_commentaire;
    private $contenu;


    function __construct($attribut){

        if(!is_object($attribut)) $attribut = (object)[];

        $this->setId_commentaire($attribut->id_commentaire ?? null);
        $this->setContenu($attribut->contenu ?? " ");
    }

    public function isValide($champ = null){

        if(null == $champ){

            $this->setId_commentaire($this->id_commentaire);
            $this->setContenu($this->contenu);

            return empty($this->listeMessageErreurActif);

        }

        $nomClasse = get_class();
        $constante = "$nomClasse::" . strtoupper($champ);
        if(!defined($constante)) return false;

        return !isset($this->listeMessageErreurActif[$champ]);

    }

    public function getListeMessageErreurActif($champ){

        return $this->listeMessageErreurActif[$champ] ?? [];

    }

    public function getId_commentaire(){

        return $this->id_commentaire;

    }

    public function setId_commentaire($id_commentaire){

        // Validation en premier

        if(null == $id_commentaire) return;

        if(!is_int(filter_var($id_commentaire, FILTER_VALIDATE_INT))){

            $this->listeMessageErreurActif['id_commentaire'][] =
                self::getListeMessageErreur()['id_commentaire-invalide'];

            $this->id_commentaire = null;

            return;

        }

        $this->id_commentaire = $id_commentaire;

    }







    public function setContenu($contenu){
        // Validation en premier

        if (empty($contenu)){

            $this->listeMessageErreurActif['contenu'][] =
                self::getListeMessageErreur()['commentaire-vide'];

            return;
        }
        if (strlen(($contenu)>200))
            {

             $this->listeMessageErreurActif['contenu'][] = self::getListeMessageErreur()['commentaire-trop-long'];

            return;
        }



        $contenuPropre=filter_var($contenu, FILTER_SANITIZE_STRING);


        // Nettoyage en second

        $this->contenu=$contenuPropre ;
    }

    public function getContenu(){

        return $this->contenu;

    }



}
