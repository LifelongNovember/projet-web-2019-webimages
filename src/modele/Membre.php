<?php
class Membre
{

   public const ID_MEMBRE = "id_membre";
   public const NOM_UTILISATEUR = "nom_utilisateur";
   public const COURRIEL = "courriel";
   public const MOT_DE_PASSE = "mot_de_passe";
   public const EST_ADMIN = "est_admin";
  private const PATERN_NOM_UTILISATEUR = "/^[a-z](?=[\w.]{3,31}$)\w*\.?\w*$/i";
   private const PATERN_MOT_DE_PASSE = '#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W)#';

   private const NOM_UTILISATEUR_NOMBRE_CARACTERE_MAXIMUM = 24;
   private const COURRIEL_NOMBRE_CARACTERE_MAXIMUM = 30;
   private const MOT_DE_PASSE_CARACTERE_MAXIMUM = 30;
   private static $LISTE_MESSAGE_ERREUR = [];
   private static $LISTE_INFORMATION_CHAMP = [];

   public static function getInformation($champ)
   {

       if(empty(self::$LISTE_INFORMATION_CHAMP))
       {

           self::$LISTE_INFORMATION_CHAMP["id_membre"] = (object)
           [
               "etiquette" => "",
               "defaut" => "",
               "indice" => "",
               "description" => "",
               "obligatoire" => null
           ];

           self::$LISTE_INFORMATION_CHAMP["nom_utilisateur"] = (object)
           [
               "etiquette" => "nom_utilisateur",
               "defaut" => "",
               "indice" => "Ex. : John (nombre maximum de caractères : " .
                           self::NOM_UTILISATEUR_NOMBRE_CARACTERE_MAXIMUM  .
                           " )",
               "description" => "Nom d'utilisateur",
               "obligatoire" => true
           ];

           self::$LISTE_INFORMATION_CHAMP["courriel"] = (object)
           [
               "etiquette" => "courriel",
               "defaut" => "",
               "indice" => "Ex. : monnom@courriel.com " .
                           "(nombre maximum de caractères : " .
                           self::COURRIEL_NOMBRE_CARACTERE_MAXIMUM .
                           " )",
               "description" => "Adresse électronique",
               "obligatoire" => true
           ];
           self::$LISTE_INFORMATION_CHAMP["mot_de_passe"] = (object)
           [
               "etiquette" => "mot_de_passe",
               "defaut" => "",
               "indice" => "Ex. : B1gf$ " .
                           "(longueur max de mot de passe : " .
                           self::MOT_DE_PASSE_CARACTERE_MAXIMUM.
                           " )",
               "description" => "Mot de passe ",
               "obligatoire" => true
           ];

           self::$LISTE_INFORMATION_CHAMP["est_admin"] = (object)
           [
               "etiquette" => "est_admin",
               "defaut" => false,
               "indice" => "",
               "description" => "est admin ",
               "obligatoire" => true
           ];

       }
         $nomClasse = get_called_class();
        $constante = "$nomClasse::" . strtoupper($champ);
        if(!defined($constante)) return null;

        return self::$LISTE_INFORMATION_CHAMP[$champ];
     }

     private static function getListeMessageErreur()
     {

        if(empty(self::$LISTE_MESSAGE_ERREUR))
        {

            self::$LISTE_MESSAGE_ERREUR =
            [
                "id_membre-invalide" =>
                    true,

                "nom-utilisateur-vide" =>
                    "Le nom d'utilisateur ne doit pas être vide",

                "nom-utilisateur-trop-long" =>
                    "Le nombre maximum de caractères pour le nom d'utilisateur est : " .
                    self::NOM_UTILISATEUR_NOMBRE_CARACTERE_MAXIMUM ,

                "nom-utilisateur-incorrect" =>
                    "Utilisez 4 à 32 caractères et commencez par une lettre)",

                    "mot-de-passe-vide" =>
                        "le mot de passe ne doit pas etre vide)",

                "courriel-vide" =>
                    "Le courriel ne doit pas être vide",

                "courriel-invalide" =>
                    "Le courriel n'est pas valide",

                "courriel-trop-long" =>
                    "Le nombre maximum de caractères pour le courriel est : " .
                    self::COURRIEL_NOMBRE_CARACTERE_MAXIMUM,

                "est_admin-invalide" =>
                    "est_admin doit etre un booleen"

            ];

        }

        return self::$LISTE_MESSAGE_ERREUR;

    }

    private static function validerNomUtilisateur($nom_utilisateur)
    {


       if (preg_match(self::PATERN_NOM_UTILISATEUR, $nom_utilisateur)) return true;

       return false;
   }
   private static function validerMotDePasse($mot_de_passe)
   {


      if (preg_match(self::PATERN_MOT_DE_PASSE, $mot_de_passe)) return true;

      return false;
  }

   private $listeMessageErreurActif = [];

    private $nom_utilisateur;
    private $courriel;
    private $id_membre;
    private $mot_de_passe;
    private $est_admin;

    function __construct($attribut)
    {

        if(!is_object($attribut)) $attribut = (object)[];
        $this->setNom_utilisateur($attribut->nom_utilisateur ?? "");
        $this->setCourriel($attribut->courriel ?? "");
        $this->setMot_de_passe($attribut->mot_de_passe ?? "");
        $this->setId_membre($attribut->id_membre ?? null);
        $this->setEstAdmin($attribut->est_admin ?? null);

    }

    public function isValide($champ = null)
    {

        if(null == $champ){

            $this->setId_membre($this->id_membre);
            $this->setNom_utilisateur($this->nom_utilisateur);
            $this->setCourriel($this->courriel);
            $this->setMot_de_passe($this->mot_de_passe);
            $this->setEstAdmin($this->est_admin);

            return empty($this->listeMessageErreurActif);

        }

        $nomClasse = get_class();
        $constante = "$nomClasse::" . strtoupper($champ);
        if(!defined($constante)) return false;

        return !isset($this->listeMessageErreurActif[$champ]);

    }

   public function getListeMessageErreurActif($champ)
    {

       return $this->listeMessageErreurActif[$champ] ?? [];
   }

   public function getId_membre()
   {

       return $this->id_membre;
   }

   public function setId_membre($id_membre)
   {

    // Validation en premier

    if(null == $id_membre) return;

    if(!is_int(filter_var($id_membre, FILTER_VALIDATE_INT)))
    {

        $this->listeMessageErreurActif['id_membre'][] =
            self::getListeMessageErreur()['id_membre-invalide'];

        $this->id_membre = null;

        return;

    }

    $this->id_membre = $id_membre;

}

    public function getNom_utilisateur(){

        return $this->nom_utilisateur;

    }

    public function setNom_utilisateur($nom_utilisateur)
    {

        // Validation en premier

        if (empty($nom_utilisateur)){

            $this->listeMessageErreurActif['nom_utilisateur'][] =
                self::getListeMessageErreur()['nom-utilisateur-vide'];

            return;
        }

        if ( strlen($nom_utilisateur) > self::NOM_UTILISATEUR_NOMBRE_CARACTERE_MAXIMUM){

            $this->listeMessageErreurActif['nom_utilisateur'][] =
                self::getListeMessageErreur()['nom-utilisateur-trop-long'];

        }

        if ( !self::validerNomUtilisateur($nom_utilisateur) )
        {

            $this->listeMessageErreurActif['nom_utilisateur'][] =
                self::getListeMessageErreur()['nom-utilisateur-incorrect'];

        }

        // Nettoyage en second

        $this->nom_utilisateur = filter_var($nom_utilisateur, FILTER_SANITIZE_STRING);

    }

    public function getCourriel()
    {

       return $this->courriel;

   }

   public function setCourriel($courriel)
   {

       // Validation en premier

       if (empty($courriel)){

           $this->listeMessageErreurActif['courriel'][] =
               self::getListeMessageErreur()['courriel-vide'];

           return;
       }

       if ( strlen($courriel) > self::COURRIEL_NOMBRE_CARACTERE_MAXIMUM)
       {

           $this->listeMessageErreurActif['courriel'][] =
               self::getListeMessageErreur()['courriel-trop-long'];

       }

       if ( !filter_var($courriel, FILTER_VALIDATE_EMAIL) )
       {

           $this->listeMessageErreurActif['courriel'][] =
               self::getListeMessageErreur()['courriel-invalide'];

       }

       $this->courriel = $courriel;

   }
   public function getMotDePasse()
   {

      return $this->mot_de_passe;

  }

  public function setMot_de_passe($mot_de_passe)
  {

      // Validation en premier

      if (empty($mot_de_passe)
    ){

          $this->listeMessageErreurActif['mot_de_passe'][] =
              self::getListeMessageErreur()['mot-de-passe-vide'];

          return;
      }

		$this->mot_de_passe = $mot_de_passe;

  }

  public function getEstAdminInt(){
    if($this->est_admin == true)
      return 1;
    else
      return 0;
  }

  public function getEstAdmin()
  {
        return $this->est_admin;
  }

  public function setEstAdmin($est_admin)
  {
      if($est_admin == 1) $est_admin = true;
      else if($est_admin == 0) $est_admin = false;

      if(is_bool($est_admin))
      {
          $this->est_admin=$est_admin;
      }else{
          $this->listeMessageErreurActif['est_admin-invalide'][] =
              self::getListeMessageErreur()['est_admin-invalide'];

          return;
      }

  }


//
}

?>
