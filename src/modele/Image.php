<?php
class Image{
  public const ID_IMAGE = "id_image";
  public const NOM = "nom";
  public const IMAGE = "image";
  public const DESCRIPTION = "description";
  public const PRIX = "prix";

  private const VALIDATION_NOM = "/^[a-z](?=[\w.]{3,31}$)\w.?\w$/i";

  private static $LISTE_ERREUR = [];
  private static $LISTE_CHAMP = [];

  private $id_image;
  private $nom;
  private $image;
  private $description;
  private $prix;
  private $tag = [];

  private $listeErreurActive = [];
  public static function getInformationChamp(){
    if(empty(self::$LISTE_CHAMP)){
      self::$LISTE_CHAMP["id_image"] = (object)[
        "etiquette" => "id_image",
        "defaut" => "",
        "indice" => "",
        "description" => "",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["nom"] = (object)[
        "etiquette" => "nom",
        "defaut" => "",
        "indice" => "",
        "description" => "nom representatif de l'image",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["image"] = (object)[
        "etiquette" => "image",
        "defaut" => "",
        "indice" => "",
        "description" => "l'adresse de l'image sur le serveur",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["description"] = (object)[
        "etiquette" => "description",
        "defaut" => "",
        "indice" => "",
        "description" => "courte description de l'image",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["prix"] = (object)[
        "etiquette" => "prix",
        "defaut" => "",
        "indice" => "",
        "description" => "le prix a l'aquel l'image sera afficher sur le site",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["tag"] = (object)[
        "etiquette" => "tag",
        "defaut" => "",
        "indice" => "",
        "description" => "les tag a laquel l'image doit etre associer",
        "obligatoire" => null
      ];
    }

  }

  public static function getListeErreur(){
    if(empty(self::$LISTE_ERREUR)){
      self::$LISTE_ERREUR = [
        "nom-non-valide" => "Utilisez 4 à 32 caractères et commencez par une lettre. Vous pouvez utiliser des lettres, des chiffres, des traits de soulignement et un point (.)",
        "description-trop-longue" => "la description doit contenir au maximum 500 caractères",
        "description-non-valide" => "la description et non valide, elle doit contenir au moin 5 caractères",
        "id-non-valide" => "l'id de l'image n'est pas valide",
        "image-non-valide" => "image non valide",
        "prix-non-valide" => "le prix est non valide",
        "tag-non-valide" => "les tags ne son pas valide"
      ];
    }
    return self::$LISTE_ERREUR;
  }

  function __construct( $atribut){
    if(!is_object($atribut)) $atribut = (object)[];
    $this->setId_image($atribut->id_image ?? null);
    $this->setNom($atribut->nom ?? "");
    $this->setImage($atribut->image ?? "");
    $this->setDescription($atribut->description ?? "");
    $this->setPrix($atribut->prix ?? null);
    $this->setTagList($atribut->tag ?? null);
  }

  public function getTag(){
    return $this->tag;
  }

  public function setTagList($tagListe){
    if($tagListe == null || empty($tagListe)) return $this->tag = [];
    foreach ($tagListe as $tagTemp) {
      if(empty($tagTemp) || !empty($tagTemp->getListeErreurActive())){
        $listeErreurActive['tag'] = self::getListeErreur()['tag-non-valide'];
        break;
      }
    }
  }

  public function getListeErreurActive(){
    return $listeErreurActive ?? [];
  }

  public function getPrix(){
    return $this->prix;
  }

  public function setPrix($prix){
    if(!is_numeric(filter_var($prix, FILTER_VALIDATE_INT)) || $prix == null)
      $listeErreurActive['prix'] = self::getListeErreur()['prix-non-valide'];
    $this->prix = $prix;
  }

  public function getId_image(){
    return $this->id_image;
  }

  public function setId_image($id){
    if($id == null || !is_numeric(filter_var($id, FILTER_VALIDATE_INT))){
      $listeErreurActive['id_image'] = self::getListeErreur()['id-non-valide'];
    }
    $this->id_image = $id;
  }

  public function getImage(){
    return $this->image;
  }

  public function setImage($addresse){
    if(empty($addresse) || strlen($addresse) < 2){
      $listeErreurActive['image'] = self::getListeErreur()['image-non-valide'];
    }
    $this->image = filter_var($addresse, FILTER_SANITIZE_STRING);
  }

  public function setNom($texte){
    if(empty($texte) || preg_match(self::VALIDATION_NOM, $texte)){
      $listeErreurActive['nom'] = self::getListeErreur()['nom-non-valide'];
    }
    $this->nom = filter_var($texte, FILTER_SANITIZE_STRING);
  }

  public function getNom(){
    return $this->nom;
  }

  public function setDescription($texte){
    if(empty($texte) ||  strlen($texte) < 5){
      $listeErreurActive['description'] = self::getListeErreur()['description-non-valide'];
    }elseif (strlen($texte) > 500) {
      $listeErreurActive['description'] = self::getListeErreur()['description-trop-longue'];
    }
    $this->description = filter_var($texte, FILTER_SANITIZE_STRING);
  }

  public function getDescription(){
    return $this->description;
  }
}
?>
