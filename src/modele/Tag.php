<?php
class Tag{
  public const ID_TAG = "id_tag";
  public const NOM = "nom";

  private static $LISTE_ERREUR = [];
  private static $LISTE_CHAMP = [];

  private $id_tag;
  private $nom;

    private const VALIDATION_NOM = "/^[a-z](?=[\w.]{3,100}$)\w.?\w$/i";

  private $listeErreurActive = [];
  public static function getInformationChamp(){
    if(empty(self::$LISTE_CHAMP)){
      self::$LISTE_CHAMP["id_tag"] = (object)[
        "etiquette" => "id_tag",
        "defaut" => "",
        "indice" => "",
        "description" => "",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["nom"] = (object)[
        "etiquette" => "nom",
        "defaut" => "",
        "indice" => "",
        "description" => "",
        "obligatoire" => true
      ];
    }

  }

  public static function getListeErreur(){
    if(empty(self::$LISTE_ERREUR)){
      self::$LISTE_ERREUR = (object)[
        "nom-non-valide" => "Utilisez 4 à 100 caractères et commencez par une lettre. Vous pouvez utiliser des lettres, des chiffres, des traits de soulignement et un point (.)",
        "id-non-valide" => "l'id de l'tag n'est pas valide",
      ];
    }
    return self::$LISTE_ERREUR;
  }

  function __construct($atribut){
    if(!is_object($atribut)) $atribut = (object)[];
    $this->setId_tag($atribut->id ?? null);
    $this->setNom($atribut->$nom ?? "");
  }

  public function getListeErreurActive($champ){
    return $listeErreurActive[$champ] ?? [];
  }

  public function getId_tag(){
    return $this->id_tag;
  }

  public function setId_tag($id){
    if($id == null || !is_numeric(filter_var($prix, FILTER_VALIDATE_INT))){
      $listeErreurActive['id_tag'] = self::$LISTE_ERREUR['id-non-valide'];
    }
    $this->id_tag = $id;
  }

  public function setNom($texte){
    if(empty($texte) || preg_match(self::VALIDATION_NOM, $texte)){
      $listeErreurActive['nom'] = self::$LISTE_ERREUR['nom-non-valide'];
    }
    $this->nom = filter_var($texte, FILTER_SANITIZE_STRING);
  }

  public function getNom(){
    return $this->nom;
  }
}
?>
