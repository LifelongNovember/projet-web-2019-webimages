<?php
class Payment{
  public const ID_PAYMENT = "id_payment";
  public const ID_IMAGE = "id_image";
  public const ID_MEMBRE = "id_membre";
  public const HEURE_PAYMENT = "heure_payment";

  private const VALIDATION_HEURE = "/[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]/";

  private static $LISTE_ERREUR = [];
  private static $LISTE_CHAMP = [];

  private $id_payment;
  private $id_image;
  private $id_membre;
  private $heure_payment;

  private $listeErreurActive = [];
  public static function getInformationChamp(){
    if(empty(self::$LISTE_CHAMP)){
      self::$LISTE_CHAMP["id_payment"] = (object)[
        "etiquette" => "id_payment",
        "defaut" => "",
        "indice" => "",
        "description" => "",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["id_image"] = (object)[
        "etiquette" => "id_image",
        "defaut" => "",
        "indice" => "",
        "description" => "",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["id_membre"] = (object)[
        "etiquette" => "id_membre",
        "defaut" => "",
        "indice" => "",
        "description" => "",
        "obligatoire" => true
      ];
      self::$LISTE_CHAMP["heure_payment"] = (object)[
        "etiquette" => "heure_payment",
        "defaut" => "",
        "indice" => "",
        "description" => "",
        "obligatoire" => true
      ];
    }

  }

  public static function getListeErreur(){
    if(empty(self::$LISTE_ERREUR)){
      self::$LISTE_ERREUR = [
        "id-payment-non-valide" => "l'id payment n'est pas valide",
        "id-image-non-valide" => "l'id image n'est pas valide",
        "id-membre-non-valide" => "l'id membre n'est pas valide",
        "heure-payment-non-valide" => "l'id heure payment n'est pas valide"
      ];
    }
    return self::$LISTE_ERREUR;
  }

  function __construct( $atribut){
    if(!is_object($atribut)) $atribut = (object)[];
    $this->setId_payment($atribut->id_payment ?? null);
    $this->setId_image($atribut->id_image ?? null);
    $this->setId_membre($atribut->id_membre ?? null);
    $this->setHeure_payment($atribut->heure_payment ?? null);
  }

  public function getId_payment(){
    return $this->id_payment;
  }

  public function setId_payment($id){
    if($id == null || !is_numeric(filter_var($id, FILTER_VALIDATE_INT))){
      $listeErreurActive['id_payment'] = self::getListeErreur()['id-payment-non-valide'];
    }
    $this->id_payment = $id;
  }

  public function getId_image(){
    return $this->id_image;
  }

  public function setId_image($id){
    if($id == null || !is_numeric(filter_var($id, FILTER_VALIDATE_INT))){
      $listeErreurActive['id_image'] = self::getListeErreur()['id-image-non-valide'];
    }
    $this->id_image = $id;
  }

  public function getId_membre(){
    return $this->id_membre;
  }

  public function setId_membre($id){
    if($id == null || !is_numeric(filter_var($id, FILTER_VALIDATE_INT))){
      $listeErreurActive['id_membre'] = self::getListeErreur()['id-membre-non-valide'];
    }
    $this->id_membre = $id;
  }

  public function getHeure_payment(){
    return $this->heure_payment;
  }

  public function setHeure_payment($heure){
    if($heure == null || preg_match(self::VALIDATION_HEURE, $heure)){
      $listeErreurActive['heure_payment'] = self::getListeErreur()['heure-payment-non-valide'];
    }
    $this->heure_payment = filter_var($heure, FILTER_SANITIZE_STRING);
  }
}
?>
