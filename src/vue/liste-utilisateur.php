<?php

require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");

$page = (object)
    [
    "URL" => "liste-utilisateur.php",
    "style" => "liste-utilisateur.css",
    "titrePage" => _("Liste des utilisateurs"),
    "titrePrincipal" => "p2pimage.com",
    "pageGestionPersonneURL" => "profil",
    "listeUtilisateur" => [],
    "message" => "Voici la liste des utilisateur"
    ];





function afficherPage($page = null){

    if(!is_object($page)) return;

    afficherEntete($page);
    ?>

<div class="wrap">
    <main role="main">
        <section>
            <header>
                <h2><?= $page->titrePage ?? ""; ?></h2>
            </header>
        </section>

        <?php
        //Si un message est présent dans la page.
        if($message = $page->message ?? false ){
        ?>

        <div class="message"><?= $message; ?></div>

        <?php
        }
        afficherListeUtilisateurAction($page);
        ?>

    </main>
</div>
<a href="/<?= $page->navigationRetourURL ?? ""; ?>">
  <?php echo _("Retourner à la page") ?> <?= $page->navigationRetourTitre ?? ""; ?>
</a>
<?php
afficherPiedDePage($page);

 if($page->listeUtilisateur){

        //URL et paramètre de la page de destination
        //pour ajouter une personne
        $hrefAjouter = assemblerHrefAjouter($page);

    ?>

<ul>

    <?php

    foreach ($page->listeUtilisateur as $utilisateur){

        //Le texte du lien les détails d'une personne
        $textItem = $utilisateur->getNom_utilisateur();

        //URL et paramètre de la page de destination
        //pour détailler une personne
        $hrefDetailler = assemblerHrefDetailler($page, $utilisateur);

    ?>

    <li>
        <a href="/<?= $hrefDetailler; ?>"><?= $textItem; ?></a>
    </li>

    <?php
    }
    ?>

</ul>

    <?php
    }

}
require_once("../action/action-liste-utilisateur.php");
?>
