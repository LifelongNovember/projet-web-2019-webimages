<?php

require_once("../modele/Membre.php");
require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");
require_once("erreur-formulaire-fragment.php");

$page = (object)
    [
    "style" => "multipage.css",
    "titrePage" => "Inscription d'un membre",
    "titreEtape" => "Premiere étape",
    "titrePrincipal" => "P2Pimage.com",
    "navigationRetourURL" => "",
    "navigationRetourTitre'" => "",
    "messageNavigationRetour" => "",
    "isNavigationRetour" => false,
    "isPremiereEtape" => true,
    "isSecondeEtape" => false,
    "isTroisiemeEtape" => false,
    "membre" => null,

    "nom_utilisateur" => Membre::NOM_UTILISATEUR,
    "etiquetteNom_utilisateur" =>
      Membre::getInformation(Membre::NOM_UTILISATEUR)->etiquette,

     "indiceNom_utilisateur" =>
          Membre::getInformation(Membre::NOM_UTILISATEUR)->indice,

     "isNom_utilisateurObligatoire" =>
          Membre::getInformation(Membre::NOM_UTILISATEUR)->obligatoire,

    "courriel" => Membre::COURRIEL,

   "descriptionCourriel" =>
       Membre::getInformation(Membre::COURRIEL)->description,

    "etiquetteCourriel" =>
       Membre::getInformation(Membre::COURRIEL)->etiquette,

    "indiceCourriel" =>
        Membre::getInformation(Membre::COURRIEL)->indice,

    "isCourrielObligatoire" =>
        Membre::getInformation(Membre::COURRIEL)->obligatoire,

      //
      "mot_de_passe" => Membre::MOT_DE_PASSE,

     "descriptionMotDePasse" =>
         Membre::getInformation(Membre::MOT_DE_PASSE)->description,

      "etiquetteMotDePasse" =>
         Membre::getInformation(Membre::MOT_DE_PASSE)->etiquette,

      "indiceMotDePasse" =>
          Membre::getInformation(Membre::MOT_DE_PASSE)->indice,

      "isMotDePasseObligatoire" =>
          Membre::getInformation(Membre::MOT_DE_PASSE)->obligatoire,

    ];

//test
function afficherPremiereEtape($page = null){

    ?>

<!-- Champ nomUtilisateur -->
<label for="<?= Membre::NOM_UTILISATEUR; ?>" title="<?= $page->description; ?>">
    <?= $page->etiquetteNom_utilisateur; ?>
</label>

<span>
    <?= $page->indiceNom_utilisateur; ?>
</span>

<input type="text" name="<?= Membre::NOM_UTILISATEUR; ?>"
       id="<?= Membre::NOM_UTILISATEUR; ?>"
       value="<?= $page->membre->getnom_utilisateur(); ?>">

<span>
   <?= $page->isNom_utilisateurObligatoire ? "obligatoire" : ""; ?>
</span>

<?php
afficherErreurFormulaire(
   $page->membre->getListeMessageErreurActif(Membre::NOM_UTILISATEUR));

?>

<input type="hidden" name="<?= Membre::COURRIEL; ?>"
       value="<?= $page->membre->getCourriel(); ?>">

<input type="hidden" name="navigation-retour-url"
       value="<?= $page->navigationRetourURL ?? ""; ?>">

<input type="hidden" name="navigation-retour-titre"
       value="<?= $page->navigationRetourTitre ?? ""; ?>">

<input type="submit" name="action-aller-seconde-etape"
       value="Aller à la seconde étape">

   <?php
}

function afficherSecondeEtape($page = null)
{

    ?>

<input type="hidden" name="<?= Membre::NOM_UTILISATEUR; ?>"
       value="<?= $page->membre->getnom_utilisateur(); ?>">


<!-- Champ courriel -->
<label for="<?= Membre::COURRIEL; ?>"
     title="<?= $page->descriptionCourriel; ?>">
  <?= $page->etiquetteCourriel; ?>
</label>

<span>
  <?= $page->indiceCourriel; ?>
</span>
<label for="mail"> Email: </label>
<input type="email" name="<?= Membre::COURRIEL; ?>"
     id="<?= Membre::COURRIEL; ?>"
     value="<?= $page->membre->getCourriel(); ?>">

<span>
 <?= $page->isCourrielObligatoire ? "obligatoire" : ""; ?>
</span>

<?php

afficherErreurFormulaire(
  $page->membre->getListeMessageErreurActif(Membre::COURRIEL));

?>
<input type="hidden" name="<?= Membre::MOT_DE_PASSE; ?>"
       value="<?= $page->membre->getMotDePasse(); ?>">

<input type="hidden" name="navigation-retour-url"
       value="<?= $page->navigationRetourURL ?? ""; ?>">

<input type="hidden" name="navigation-retour-titre"
       value="<?= $page->navigationRetourTitre ?? ""; ?>">
       <input type="submit" name="action-retourner-premiere-etape"
              value="Retourner à la première étape">

       <input type="submit" name="action-aller-troisieme-etape"
              value="Aller à la trosième étape">


   <?php
}

function afficherTroisiemeEtape($page = null)
{

  ?>

<input type="hidden" name="<?= Membre::NOM_UTILISATEUR; ?>"
   value="<?= $page->membre->getNom_utilisateur(); ?>">

<input type="hidden" name="<?= Membre::COURRIEL; ?>"
   value="<?= $page->membre->getCourriel(); ?>">

<!-- Champ Mot de passe -->
<label for="<?= Membre::MOT_DE_PASSE; ?>"
 title="<?= $page->descriptionMotDePasse; ?>">
<?= $page->etiquetteMotDePasse; ?>
</label>

<span>
<?= $page->indiceMotDePasse; ?>
</span>

<label>Mot de passe
    <input type="password" value="<?=$page->membre->getMotDePasse();?>" name="<?=Membre::MOT_DE_PASSE?>"  required>
  </label>
  <label>Confirmer Mot de passe
    <input type="password" value="<?=$page->membre->getMotDePasse();?>" name="confirmerMotDePasse" required>
  </label>


<span>
<?= $page->isCourrielObligatoire ? "obligatoire" : ""; ?>
</span>

<?php

afficherErreurFormulaire(
  $page->membre->getListeMessageErreurActif(Membre::MOT_DE_PASSE));

?>

<input type="hidden" name="navigation-retour-url"
   value="<?= $page->navigationRetourURL ?? ""; ?>">

<input type="hidden" name="navigation-retour-titre"
   value="<?= $page->navigationRetourTitre ?? ""; ?>">

   <input type="submit" name="action-retourner-seconde-etape"
        value="Retourner à la seconde étape">


<input type="submit" name="action-inscrire"
   value="Terminer l'inscription">

<?php
}

function afficherPage($page = null){

    if(!is_object($page)) $page = (object)[];

    //Redirection vers la page de retour avec un message de réussite.
    if($page->isNavigationRetour ?? false &&
       $page->navigationRetourURL ?? false){

        $location = $page->navigationRetourURL .
        "?" .
        "message=" .
        $page->messageNavigationRetour;

        header("Location: " . $location);

        exit;
    }

    afficherEntete($page);

    ?>

<div class="wrap">

    <main role="main">

        <section>

            <header>

                <h2><?= $page->titreEtape ?? ""; ?></h2>

            </header>

        </section>

        <form method="post" action="inscription.php">

            <?php
            if($page->isPremiereEtape ?? false){
                afficherPremiereEtape($page);
            }

            if($page->isSecondeEtape ?? false){
                afficherSecondeEtape($page);
            }

            if($page->isTroisiemeEtape ?? false){
                afficherTroisiemeEtape($page);
            }
            ?>

        </form>



    </main>

</div>

    <?php

    afficherPiedDePage($page);

}
require_once("../action/action-inscription.php");
