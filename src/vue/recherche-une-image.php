<?php

//require_once("Image.php");
require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");

$page = (object)
  [
    "style"=>"recherche-une-image.css",
    "titrePage" => _("Chercher une image"),
    "titrePrincipal" => "p2pImage",
    "navigationRetourURL" => "",
    "navigationRetourTitre'" => "",
    "messageNavigationRetour" => "",
    "isNavigationRetour" => false,
    "image" => null
  ];

function afficherPage($page = null){

  if(!is_object($page)) $page = (object)[];

  //Redirection vers la page de retour avec un message de réussite.
  if($page->isNavigationRetour ?? false &&
    $page->navigationRetourURL ?? false){

    $location = $page->navigationRetourURL .
      "?" .
      "message=" .
      $page->messageNavigationRetour;

    header("Location: " . $location);

    exit;
  }

  afficherEntete($page);

?>

<section>

  <header>

    <h2><?= $page->titre ?? ""; ?></h2>

  </header>

</section>

<section class="conteneur_formulaire">

<?php /* 
  <div class="inline">

  <input type="hidden" name="navigation-retour-url" value="<?= $page->navigationRetourURL ?? ""; ?>">

  <input type="hidden" name="navigation-retour-titre" value="<?= $page->navigationRetourTitre ?? ""; ?>">

  </div>
  <div id="messages"></div>

  <div class="center">
    <a href="<?= $page->navigationRetourURL ?? ""; ?>">
      <button type="button"><?php _("Retourner à la page")?> <?= $page->navigationRetourTitre ?? ""; ?></button>
    </a>
    </div>

*/?>

<form method="post" action="/accueil">
  <div class="inline">
  <input type="text" name="recherche" id="barreRecherche-recherche" placeholder="Titre de l'image" class="champ" autocomplete="off">
  <div id="suggestion-recherche" class="suggestions"></div>
  </div>
  <label for="prixMax"><?php echo _("Prix maximum:") ?> </label>
  <input type="range" name="prix" min="0" max="10000" class="champ"  oninput="outPrix.value = prix.value">
   <output name="ageOutputName" id="outPrix">5000</output>
    <input type="submit" name="action-rechercher-une-image" value=<?php echo _("Rechercher l'image") ?> class="champ submit">
  </form>

  </section>
  <?php

    afficherPiedDePage($page);

  }

  require_once("../action/action-recherche-une-image.php");
  ?>
