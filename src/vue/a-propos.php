<?php

require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");

$page = (object)
  [
    "style" => "a-propos.css",
    "titrePage" => _("A propos"),
    "titrePrincipal" => "p2pImage",
    "navigationRetourURL" => "",
    "navigationRetourTitre'" => "",
    "messageNavigationRetour" => "",
    "isNavigationRetour" => false,
    "image" => null
  ];

function afficherPage($page = null){

  if(!is_object($page)) $page = (object)[];

  //Redirection vers la page de retour avec un message de réussite.
  if($page->isNavigationRetour ?? false &&
    $page->navigationRetourURL ?? false){

    $location = $page->navigationRetourURL .
      "?" .
      "message=" .
      $page->messageNavigationRetour;

    header("Location: " . $location);

    exit;
  }

  afficherEntete($page);

?>


<section>
  <header>
    <h2><?= $page->titre ?? ""; ?></h2>
  </header>
</section>
<section>
  <div class="wrapper">
  <p><?php echo _("p2pImage.com est un site de vente d'images entre particuliers.")?></p>
  <p><?php echo _("Photographes amateurs, dessinateurs, peintres ou experts photoshop, vous pouvez vendre vos oeuvres au monde entier et trouver de nouvelles sources d'inspiration ou de la matière à revisiter.")?></p>
  </div>
</section>

<?php

  afficherPiedDePage($page);
}

require_once("../action/action-a-propos.php");
?>
