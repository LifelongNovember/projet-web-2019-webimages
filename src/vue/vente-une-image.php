<?php
require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");

$page = (object)
  [
    "style"=>"vente-une-image.css",
    "titrePage" => _("Vendre une image"),
    "titrePrincipal" => "p2pImage",
    "navigationRetourURL" => "",
    "navigationRetourTitre'" => "",
    "messageNavigationRetour" => "",
    "isNavigationRetour" => false,
  ];

function afficherPage($page = null){

  if(!is_object($page)) $page = (object)[];

  //Redirection vers la page de retour avec un message de réussite.
  if($page->isNavigationRetour ?? false &&
    $page->navigationRetourURL ?? false){

    $location = $page->navigationRetourURL .
      "?" .
      "message=" .
      $page->messageNavigationRetour;

    header("Location: " . $location);

    exit;
  }

  afficherEntete($page);

?>


<section>

  <header>

    <h2><?= $page->titre ?? ""; ?></h2>

  </header>

</section>

<section class="conteneur_formulaire">

<form method="post" enctype="multipart/form-data" action="vente-une-image.php" >

  <div class="inline">
  <input type="text" name="titre" placeholder="<?= _("Titre de l'image") ?>" class="champ">

  <label for="prix"><?php echo _("Prix :") ?> </label>
  <input type="number" name="prix" min="0" class="champ">



  <input type="hidden" name="id_image" value="<?= $image->id_image ?? ""; ?>">

  <input type="hidden" name="navigation-retour-url" value="<?= $page->navigationRetourURL ?? ""; ?>">

  <input type="hidden" name="navigation-retour-titre" value="<?= $page->navigationRetourTitre ?? ""; ?>">

  <input type="file" name="imageInput"  id="fileselect" class="champ">

  </div>


<div id="messages"></div>

<div class="center">
  <label for="tags"><?php echo _("Tags :") ?> </label>

  <input type="text" name="tags" placeholder="<?php echo _("tag1, tag2, ...") ?>" class="champ">

  <label for="description"><?php echo _("Description :") ?> </label>
  <textarea name="description" class="champ"></textarea>

  <input type="submit" name="action-vente-une-image" value="<?php echo _("Vendre l'image") ?>" class="champ submit">

  <a href="<?= $page->navigationRetourURL ?? ""; ?>">
    <button type="button"><?php echo _("Retourner à la page") ?> <?= $page->navigationRetourTitre ?? ""; ?></button>
  </a>
</div>

</form>

</section>

<?php

  afficherPiedDePage($page);

}

require_once("../action/action-vente-une-image.php");
?>


<script type="text/javascript" src="../interaction/dragndrop.js" ></script>
