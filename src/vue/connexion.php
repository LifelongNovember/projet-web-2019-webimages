<?php
require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");
require_once("../modele/Membre.php");
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$page = (object)
    [
    "style" => "acceuil.css",
    "titrePage" => _("Connexion"),
    "titrePrincipal" => "p2pImage",
    "itemMenuActif" => "accueil",
    "image" => null
    ];
    function afficherPage($page = null){

    // En cas d'erreur avec le paramètre $page, un objet $page vide est créé
    if(!is_object($page)) $page = (object)[];

    afficherEntete($page);
 ?>
<div>
    <form method="post" action="connexion.php">
      <h2 ><?php echo _("Se connecter en tant que membre") ?></h2>
      <label for="<?php Membre::NOM_UTILISATEUR ?>">Nom d'utilisateur</label>
        <input type="text" placeholder="Pseudo" name="<?=Membre::NOM_UTILISATEUR?>"> 
      <label for="<?php Membre::MOT_DE_PASSE ?>"><?php echo _("Mot de passe") ?></label>
        <input type="password" placeholder="mote de passe" name="<?=Membre::MOT_DE_PASSE?>">
      <p><input type="submit" value="<?php echo _("S'identifier")?>"></p>
      <p class="text-center"><a href="#"><?php echo _("Vous avez oublié votre mot de passe ?") ?></a></p>
    </form>
</div>
<?php
afficherPiedDePage($page);
}
require_once("../action/action-connexion.php");
?>
