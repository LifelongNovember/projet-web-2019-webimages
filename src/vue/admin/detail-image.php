<?php
  require_once("../fragment-entete.php");
  require_once("../fragment-pied-de-page.php");

  $page = (object) [
    "style" => "detail.css",
    "titrePage" => _("Detail image"),
    "titrePrincipale" => "p2pimage",
    "navigationAcceuil" => "index.php",
    "style" => "detail.css",
    "image" => null,
    "listeCommentaire" => null
  ];
function afficherPage($page = null){
  if(!is_object($page)) return;
  afficherEntete($page);
?>
  <img src="<?=$page->image->getImage()?>">
  <p><?=$page->image->getDescription()?></p>

  <form method="post" action="detail-image.php">
    <label id="labelCommentaire" for="commentaire"><?php echo _("commente")?></label>
    <textarea name="commentaire" id="Commentaire" name="commentaire"></textarea>
    <input id="soummetreCommentaire" type="submit" value=<?php echo _("soumettre") ?>>
  </form>

  <?php foreach ($page->listeCommentaire as $commentaire): ?>
    <p class="commentaire"> <?= $commentaire->getContenu();  ?></p>
        <?php if(isset($_SESSION['profil'])&& $_SESSION['admin']==1){
    ?>
        <form  method="post">
            <input id="id_commentaire" type="hidden" value="<?= $commentaire->getId_commentaire(); ?>">
            <input id="supprimercommentaire" type="submit" value="<?php echo _("supprimer") ?>">
        </form>
       <?php } ?>
  <?php endforeach;
        if(!empty($_SESSION['profil'])):?>
        <a id="lien-achat" href="achat-une-image.php?id_image=<?=$page->image->getId_image()?>" >acheter cette image pour <?=$page->image->getPrix()?>$CAD</a>
<?php   endif;
  afficherPiedDePage($page);
}
require_once("../action/admin/action-detail-image.php");
