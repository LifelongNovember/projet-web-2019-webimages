<?php

require_once("../fragment-entete.php");
require_once("../fragment-pied-de-page.php");

$page = (object)
    [
    "URL" => "liste-image.php",
    "style" => "liste-image.css",
    "titrePage" => _("Liste des images"),
    "titrePrincipal" => "p2pimage.com",
    "pageSuppressionImageURL" =>"suppression-image.php",
    "pageAjoutImageURL" => "vente-une-image.php",
    "pageGestionImageURL" => "detail-image.php",
    "listeImage" => [],
    "message" => _("Voici la liste des images")
    ];





function afficherPage($page = null){

    if(!is_object($page)) return;

    afficherEntete($page);
    ?>

<div class="wrap">
    <main role="main">
        <section>
            <header>
                <h2><?= $page->titrePage ?? ""; ?></h2>
            </header>
        </section>

        <?php
        //Si un message est présent dans la page.
        if($message = $page->message ?? false ){
        ?>

        <div class="message"><?= $message; ?></div>

        <?php
        }
        afficherListeImageAction($page);
        ?>

    </main>
</div>
<a href="<?= $page->navigationRetourURL ?? ""; ?>">
  <?php echo _("Retourner à la page") ?> <?= $page->navigationRetourTitre ?? ""; ?>
</a>
<?php
afficherPiedDePage($page);

}
require_once("../action/action-liste-image.php");
?>
