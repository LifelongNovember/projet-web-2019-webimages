<?php

require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");


$page = (object)
    [
    "style" => "accueil.css",
    "titrePage" => _("Acceuil"),
    "titrePrincipal" => "p2pImage",
    "itemMenuActif" => "accueil",
    "listeImage" => null,
    ];
    function afficherPage($page = null){

    // En cas d'erreur avec le paramètre $page, un objet $page vide est créé
    if(!is_object($page)) $page = (object)[];

    afficherEntete($page);
?>


  <div class="bandeau">
      <div>
      <h1>p2pimage</h1>
      <p>Achat et vente d'image en direct</p>
      <form method="post" action="/accueil" autocomplete="off">
        <div class="inline">
          <input id="barreRecherche-index" type="text" name="recherche" placeholder="Chercher une image" class="champ">
          <div id="suggestion-index" class="suggestions"></div>
        </div>
        <input type="submit" name="bouton" value=<?php echo _("Rechercher l'image") ?> class="champ submit">
      </form>
      </div>
      <div class="index-options">
        <div class="buttons">
          <a href="/liste/images"><input type="button" class="button" value="<?php echo _("Images") ?>" /></a>
          <a href="/liste/utilisateurs"><input type="button" class="button" value="<?php echo _("Utilisateurs") ?>"></a>
        </div>
    </div>
  </div>
  <div class="listeImage">
    <?php foreach ($page->listeImage as $image) {?>
      <div class="image">
        <a class="lien" href="/detail?id_image=<?=$image->getId_image()?>"><?=$image->getNom()?></a>
        <a class="lienAffichage" href="/detail?id_image=<?=$image->getId_image()?>"><img class="affichage" src="<?=$image->getImage()?>"></a>
      </div>
    <?php }

afficherPiedDePage($page);
}
require_once("../action/action-accueil.php");
?>
