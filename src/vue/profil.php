<?php
require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");
require_once("../modele/Membre.php");

$page = (object)
  [
    "style" => "profil.css",
    "titrePage" => _("Affichage du profil"),
    "titrePrincipal" => "P2PImage",
    "navigationRetourURL" => "",
    "navigationRetourTitre" => "",
    "messageNavigationRetour" => "",
    "isNavigationRetour" => false,
    "isSupprimerPersonne" => false,
    "isAjouterPersonne" => false,
    "isModifierPersonne" => false,
    "isformulaireEditable" => false,
    "personne" => null,
    "champs" =>
    [
      Membre::NOM_UTILISATEUR => Membre::NOM_UTILISATEUR,
      Membre::COURRIEL => Membre::COURRIEL,
      Membre::MOT_DE_PASSE => Membre::MOT_DE_PASSE,
    ],
    "listeTransaction"=>null
  ];

function afficherPage($page = null) {

  if(!is_object($page)) $page = (object)[];

  //Redirection vers la page de retour avec un message de réussite.
  if($page->isNavigationRetour ?? false &&
    $page->navigationRetourURL ?? false) {

    $location = $page->navigationRetourURL .
    "?" .
    "message=" .
    $page->messageNavigationRetour;

    header("Location: " . $location);

    exit;
  }

  afficherEntete($page);
  $page->personne = recupererPersonne();
  if(empty($page->personne))$page->personne = new Membre( $_SESSION['profil']);

  //var_dump($_SESSION['profil']);
?>

<div class="wrap">
  <main>
    <section>
      <header>
        <h3><?php echo _("Informations") ?></h3>
      </header>
      <img src="<?= $page->personne->avatar ?? ""; ?> " class="avatar_membre" />
        <form method="post" class="infos_membre" action="profil.php?id_membre=<?=$page->personne->getId_membre()?>">

            <fieldset
                <?= ($page->isformulaireEditable ?? false) ?
                    "" : "disabled='disabled'"; ?>>
          <?php if(!empty($page->personne)): ?>

            <label for="<?=Membre::NOM_UTILISATEUR?>">Nom</label>
            <input type="text" name="<?=Membre::NOM_UTILISATEUR?>" id="champNom" value=<?=$page->personne->getNom_utilisateur()?>>
            <label for="<?=Membre::COURRIEL?>">Courriel</label>
            <input type="text" name="<?=Membre::COURRIEL?>" id="champCouriel" value=<?=$page->personne->getCourriel()?>>
            <label for="<?=Membre::MOT_DE_PASSE?>">Mot de passe</label>
            <input type="password" name="<?=Membre::MOT_DE_PASSE?>" id="champMotDePasse" value=<?=$page->personne->getMotDePasse()?>>
          <?php else: ?>

            <label for="<?=Membre::NOM_UTILISATEUR?>">Nom</label>
            <input type="text" name="<?=Membre::NOM_UTILISATEUR?>" id="champNom" value="">
            <label for="<?=Membre::COURRIEL?>">Courriel</label>
            <input type="text" name="<?=Membre::COURRIEL?>" id="champCouriel" value="">
            <label for="<?=Membre::MOT_DE_PASSE?>">Mot de passe</label>
            <input type="password" name="<?=Membre::MOT_DE_PASSE?>" id="champMotDePasse" value="">
          <?php endif; ?>

            <!-- Champ id_personne
            <input type="hidden" name="<?= $page->id_personne; ?>"
                   value="<?= $page->personne->id; ?>">
-->
            <input type="hidden" name="navigation-retour-url"
                   value="<?= $page->navigationRetourURL ?? ""; ?>">

            <input type="hidden" name="navigation-retour-titre"
                   value="<?= $page->navigationRetourTitre ?? ""; ?>">

           </fieldset>

            <?php

            if($page->isAjouterPersonne ?? false){
            ?>

            <input type="submit" name="action-ajouter-personne"
                   value=<?php echo _("Ajouter la personne") ?>>

            <?php
            }
            if($page->isModifierPersonne ?? false){
            ?>

            <input type="submit" name="action-modifier-personne"
                   value=<?php echo _("Modifier la personne") ?>>

            <?php
            }

            if($page->isSupprimerPersonne ?? false){
            ?>

            <input type="submit" name="action-supprimer-personne"
                   value=<?php echo _("Supprimer la personne") ?>>

            <?php
            }
            ?>

        </form>
        <a href="/profil?id_membre=<?=$page->personne->getId_membre()?>&action-navigation=modifier-utilisateur">modifier vos information</a>
    </section>
    <hr>
    <section id="transaction">
      <h3> Transactions </h3>
      <table class="transaction">
        <tr><td><p>Date</p></td><td><p>Image</p></td><td><p>montan</p></td></tr>
        <?php foreach($page->listeTransaction as $transaction):?>
          <tr><td><p><?=$transaction->getHeure_payment()?></p></td><td><a href="/detail?id_image=<?=$transaction->getId_image()?>">Image</a></td><td><p><?=$transaction->montant?></p></td></tr>
        <?php endforeach; ?>
      </table>
    </section>
    <section class="conteneur_gallerie">
      <header>
        <h3>Galerie</h3>
      </header>
      <div class="gallerie">
      </div>
      <a href="/
<?= $page->personne->pseudo ?? ""; ?>/gallerie">
<!--?= sizeof($page->personne->galerie) > 3 ? sizeof($page->personne->galerie) - 3 . _(" images supplémentaires") : _("Voir la gallerie complète");
// TODO : Routage ?-->
      </a>
    </section>
  </main>
</div>

<?php
  afficherPiedDePage($page);

}

require_once("../action/action-profil.php");
