<?php
function afficherEntete($page = null){

  if(!is_object($page)) return;

?>

<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="../decoration/header-footer.css" rel="stylesheet">
<?php if(isset($page->style)){ ?>
  <link href="../decoration/<?= $page->style; ?>" rel="stylesheet" media="all">
<?php } ?>
  <title><?=$page->titrePrincipal ?? "titre Placeholder"?></title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136240953-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-136240953-1');
</script>

</head>
<body>
  <div id="contenuTout">
  <header>
<!--    <h1 id="titrePage"><?=$page->titrePage ?? "titre Placeholder"?></h1> -->
    <?php if(isset($_SESSION['profil'])){ ?>
      <nav class="barreNavigation" id="navbar">
        <div class="icon">
          <button class="hamburger" onclick="hamburger()">
              <i class="fa fa-bars"></i>
          </button>
        </div>
      <form method="post" action="/accueil" autocomplete="off" class="recherche-nav">
        <div class="inline">
          <input id="barreRecherche" type="text" name="recherche" placeholder="Chercher une image" class="champ">
          <div id="suggestion" class="suggestions"></div>
        </div>
        <input type="submit" name="bouton" value=<?php echo _("Rechercher l'image") ?> class="champ submit">
      </form>
        <a class="lienBarreNavigation" href="/accueil"><?php echo _("Accueil") ?></a>
        <a class="lienBarreNavigation" href="/blog"><?php echo _("Blog") ?></a>
        <a class="lienBarreNavigation" href="/recherche"><?php echo _("Recherche") ?></a>
        <a class="lienBarreNavigation" href="/profil"><?php echo _("Profil") ?></a>
        <a class="lienBarreNavigation" href="/vente"><?php echo _("Mettre une image en vente") ?></a>
      </nav>
    <?php }else{ ?>
      <nav class="barreNavigation" id="navbar">
        <div class="icon">
          <button class="hamburger" onclick="hamburger()">
              <i class="fa fa-bars"></i>
          </button>
        </div>
      <form method="post" action="/accueil" autocomplete="off" class="recherche-nav">
        <div class="inline">
          <input id="barreRecherche" type="text" name="recherche" placeholder="Chercher une image" class="champ">
          <div id="suggestion" class="suggestions"></div>
        </div>
        <input type="submit" name="bouton" value=<?php echo _("Rechercher l'image") ?> class="champ submit">
      </form>
        <a class="lienBarreNavigation" href="/accueil"><?php echo _("Accueil") ?></a>
        <a class="lienBarreNavigation" href="/blog"><?php echo _("Blog") ?></a>
        <a class="lienBarreNavigation" href="/recherche"><?php echo _("Recherche") ?></a>
        <a class="lienBarreNavigation" href="/connexion"><?php echo _("Connexion") ?></a>
        <a class="lienBarreNavigation" href="/inscription"><?php echo _("Inscription") ?></a>
      </nav>
    <?php }?>
  </header>
  <div id="contenu">
<?php

}
?>
