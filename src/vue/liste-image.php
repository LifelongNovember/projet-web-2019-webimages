<?php

require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");

$page = (object)
    [
    "URL" => "liste-image.php",
    "style" => "liste-image.css",
    "titrePage" => _("Liste des images"),
    "titrePrincipal" => "p2pimage.com",
    "pageSuppressionImageURL" =>"suppression-image.php",
    "pageAjoutImageURL" => "vente",
    "pageGestionImageURL" => "detail",
    "listeImage" => [],
    "message" => _("Voici la liste des images")
    ];





function afficherPage($page = null){

    if(!is_object($page)) return;

    afficherEntete($page);
    ?>

<div class="wrap">
    <main role="main">
        <section>
            <header>
                <h2><?= $page->titrePage ?? ""; ?></h2>
            </header>
        </section>

        <?php
        //Si un message est présent dans la page.
        if($message = $page->message ?? false ){
        ?>

        <div class="message"><?= $message; ?></div>

        <?php
        }
        afficherListeImageAction($page);
       
		if($page->listeImage){

        //URL et paramètre de la page de destination
        //pour ajouter une image
        $hrefAjouter = assemblerHrefAjouter($page);

    ?>

	<a href="<?= $hrefAjouter; ?>">Ajouter une Image</a>

<div class="listeImage">

    <?php

    foreach ($page->listeImage as $image){


        $UrlItem = $image->getImage();
        $altItem = $image->getDescription();
        $titreItem = $image->getNom();

        //URL et paramètre de la page de destination
        //pour détailler une image
        $hrefDetailler = assemblerHrefDetailler($page, $image);


        //URL et paramètre de la page de destination
        //pour supprimer une personne
        $hrefSupprimer = assemblerHrefSupprimer($page, $image);

    ?>

    <div class="image">
        <a href="/<?= $hrefDetailler; ?>"><img src="/<?= $UrlItem; ?>" alt="<?= $altItem; ?>"/><?=$titreItem?></a>
       
    </div>

    <?php
    }
    ?>

</div>

    <?php
    }
	?>
    </main>
</div>
<a href="<?= $page->navigationRetourURL ?? ""; ?>">
  <?php echo _("Retourner à la page") ?> <?= $page->navigationRetourTitre ?? ""; ?>
</a>
<?php
afficherPiedDePage($page);

}
require_once("../action/action-liste-image.php");
?>
