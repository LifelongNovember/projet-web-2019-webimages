<?php

//require_once("Image.php");
require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");

$page = (object)
    [
    "style"=>"achat-une-image.css",
    "titrePage" => _("Achat d'une image"),
    "titrePrincipal" => "p2pImage",
    "navigationRetourURL" => "",
    "navigationRetourTitre'" => "",
    "messageNavigationRetour" => "",
    "isNavigationRetour" => false,
    "image" => null,
    ];

function afficherPage($page = null){

    if(!is_object($page)) $page = (object)[];

    //Redirection vers la page de retour avec un message de réussite.
    if($page->isNavigationRetour ?? false &&
       $page->navigationRetourURL ?? false){

        $location = $page->navigationRetourURL .
        "?" .
        "message=" .
        $page->messageNavigationRetour;

        header("Location: " . $location);

        exit;
    }

    afficherEntete($page);

    ?>

<div class="wrap">
    <main role="main">
        <section>
            <header>
                <h2><?=$page->image->getNom() ?? "placeholder"?></h2>
            </header>
        </section>

        <div class="formulaireAchat">

          <img src="<?=$page->image->getImage() ?? "placeholder"?>">

          <form action="../action/action-charger-payment.php" method="POST">
            <input type="hidden" name="id_image" value="<?=$page->image->getId_image()?>">
            <script
              src="https://checkout.stripe.com/checkout.js" class="stripe-button"
              data-key="pk_test_0jDIhvZhofyXsbpsq7wnH7Hx"
              data-amount="<?=$page->image->getPrix()*100?>"
              data-name="Demo Site"
              data-description="<?=$page->image->getNom() ?? "placeholder"?>"
              data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
              data-locale="auto"
              data-currency="cad">
            </script>
          </form>
        </div>

        <a href="<?= $page->navigationRetourURL ?? ""; ?>">
            <?php echo _("Retourner à la page ")?> <?= $page->navigationRetourTitre ??""; ?>
        </a>

    </main>

</div>

    <?php

    afficherPiedDePage($page);

}

require_once("../action/action-achat-une-image.php");
