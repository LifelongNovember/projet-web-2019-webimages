<?php
  require_once("fragment-entete.php");
  require_once("fragment-pied-de-page.php");

  $page = (object) [
    "titrePage" => _("Suppression image"),
    "titrePrincipale" => "p2pimage",
    "navigationAcceuil" => "index.php",
    "style" => "suppression-image.css",
    "image" => null
  ];
function afficherPage($page = null){
  if(!is_object($page)) return;
  afficherEntete($page);
?>
<div class="form-supression">
  <img src="<?=$page->image->getImage()?>">

  <form  method="post" action="suppression-image.php">


     <a href="<?= $page->navigationRetourURL ?? ""; ?>"><button type='button'><?php echo _("Annuler la suppression") ?></button></a>
     <input type="hidden" name="confirmer" id="confirmer" value="true">
    <input type="hidden" name="id_image" value="<?=$page->image->getId_image()?>">
    <input id="supprimerImage" name="supprimerImage" type="submit" value=<?php echo _("confirmer la suppression") ?>>
  </form>
    </div>
<?php

    //afficherPiedDePage($page);

}
require_once("../action/action-suppression-image.php");
 ?>
