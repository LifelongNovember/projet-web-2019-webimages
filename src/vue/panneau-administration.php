<?php
require_once("fragment-entete.php");
require_once("fragment-pied-de-page.php");
require_once("../modele/Payment.php");

$page = (object)
    [
    "URL" => "panneau-administration.php",
    "style" => "panneau-admnistration.css",
    "titrePage" => _("Panneau d'administration"),
    "titrePrincipal" => "p2pimage.com",
    "pageGestionUtilisateurURL" => "liste/utilisateurs",
    "pageGestionImageURL" => "liste/images",
    "pageGestionCommentaireURL" =>"liste-commentaire.php",
    "message" => _("Bienvenu sur le paneau d'administration"),
    "listeTransaction" => null,
    "hrefgestionUtilisateur" => "",
    "hrefgestionImage" => ""
    ];

function afficherPage($page = null){

    if(!is_object($page)) return;

    afficherEntete($page);
    ?>

    <div class="wrap">
        <main role="main">
            <section>
                <header>
                    <h2><?= $page->titrePage ?? ""?></h2>
                </header>
            </section>

            <?php
            //Si un message est présent dans la page.
            if($message = $page->message ?? false ):
            ?>

            <div class="message"><?= $message; ?></div>
            <a href="<?= $page->hrefgestionUtilisateur; ?>"><?= _("Gerer les utilisateurs") ?></a>

            <a href="<?= $page->hrefgestionImage; ?>"><?= _("Gerer les images") ?></a>
          <?php endif; ?>

        </main>
    </div>
    <section id="transaction">
      <h3> Transactions </h3>
      <table class="transaction">
        <tr><td><p>Date</p></td><td><p>Acheteur</p></td><td><p>Image</p></td><td><p>montan</p></td></tr>
        <?php foreach($page->listeTransaction as $transaction):?>
          <tr><td><p><?=$transaction->getHeure_payment()?></p></td><td><a href="/profil?id_membre=<?=$transaction->getId_membre()?>">Acheteur</a></td><td><a href="/detail?id_image=<?=$transaction->getId_image()?>">Image</a></td><td><p><?=$transaction->montant?></p></td></tr>
        <?php endforeach; ?>
      </table>
    </section>

    <?php
    afficherPiedDePage($page);
  }
    require_once("../action/action-panneau-administration.php");

?>
