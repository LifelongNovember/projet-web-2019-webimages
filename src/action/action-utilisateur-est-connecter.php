<?php
session_start();
require_once("../accesseur/AccesseurMembre.php");
function retourAcceuil(){
  header("location: /connexion.php");
  die();
}

if(empty($_SESSION["profil"])) retourAcceuil();

$accesseurMembre = new AccesseurMembre();
$utilisateur = new Membre($_SESSION["profil"]);
//var_dump($utilisateur);

if(!$accesseurMembre->recupererMembreParNom($utilisateur)->isValide()) retourAcceuil();
?>
