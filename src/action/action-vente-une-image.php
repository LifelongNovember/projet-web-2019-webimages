<?php
include_once("../action/action-utilisateur-est-connecter.php");
require_once("../accesseur/AccesseurImage.php");
require_once("../modele/Image.php");

function ajouterImage($image){
  $acesseurImage = new AcesseurImage();
    $chemin=enregistrerSurServeur();
if( $chemin != false){
    $image->setImage($chemin);
   if($acesseurImage->ajouterImage($image))return true;
}

  return false;
}

function enregistrerSurServeur(){

    $uploads_dir="../upload";
     $tmp_name = $_FILES["imageInput"]["tmp_name"];
    $file=$_FILES["imageInput"]["name"];
    $ext =".jpeg";
    //TODO recuperation de L'extension de l'image
    $name =  uniqid('img_', true).$ext;

    $path="$uploads_dir/$name";
        if(move_uploaded_file($tmp_name, "$uploads_dir/$name"))return $path ;

    return false ;
}

  if($_GET["navigation-retour-url"] ?? false &&
     $_GET["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_GET["navigation-retour-url"];
      $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

  }else if($_POST["navigation-retour-url"] ?? false &&
           $_POST["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_POST["navigation-retour-url"];
      $page->navigationRetourTitre = $_POST["navigation-retour-titre"];

  }

  if($_GET["id_utilisateur"] ?? false){

      if(!$page->image = recupererImage(new Image((object) $_GET))){

          $page->messageNavigationRetour = _("Utilisateur non trouvée").
          $page->isNavigationRetour = true;

      }

  }

  if(isset($_POST['titre'])) {

    $image = new Image((object)[
      Image::NOM=>$_POST['titre'],
      Image::DESCRIPTION=>$_POST['description'],
      Image::IMAGE=>'',
      Image::PRIX=>$_POST['prix']
    ]);

    if(ajouterImage($image)){

          $page->messageNavigationRetour =
              _("mise en vente réussie");

          $page->isNavigationRetour = true;

      }
      else{

          $page->messageAction =
              _("Erreur lors de la vente");

          $page->titre = _("Vendre une image");
      }
  }
  afficherPage($page);
 ?>
