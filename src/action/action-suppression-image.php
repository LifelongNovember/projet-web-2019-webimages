<?php
include_once("action/action-utilisateur-est-connecter.php");

if($_GET["navigation-retour-url"] ?? false &&
     $_GET["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_GET["navigation-retour-url"];
      $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

  }else if($_POST["navigation-retour-url"] ?? false &&
           $_POST["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_POST["navigation-retour-url"];
      $page->navigationRetourTitre = $_POST["navigation-retour-titre"];

  }
     if(isset($_GET['id_image'])){
       $id_image=$_GET['id_image'];
     }elseif(isset($_POST['id_image'])){
        $id_image=$_POST['id_image']; 
     }

  require_once("../accesseur/AccesseurImage.php");
  $accesseurImage = new AcesseurImage();
$image= new Image((object)[
      Image::ID_IMAGE=>$id_image
    ]);
  $page->image = $accesseurImage->recupererImage($image->getId_image());
  afficherPage($page);
  if(!empty($_POST['confirmer'])){
    $accesseurImage->suprimerImage($page->image);
    header("location:liste-image.php ");
  }
?>
