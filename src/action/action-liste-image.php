<?php
include_once("../action/action-utilisateur-est-connecter.php");
require_once "../accesseur/AccesseurImage.php";
if($_GET["navigation-retour-url"] ?? false &&
     $_GET["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_GET["navigation-retour-url"];
      $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

  }else if($_POST["navigation-retour-url"] ?? false &&
           $_POST["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_POST["navigation-retour-url"];
      $page->navigationRetourTitre = $_POST["navigation-retour-titre"];

  }

function assemblerHrefAjouter($page){

    $hrefAjouter = $page->pageAjoutImageURL."?" .
        http_build_query(
        [
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefAjouter;

}

function assemblerHrefDetailler($page, $image){

    $hrefDetailler = $page->pageGestionImageURL . "?" .
        http_build_query(
        [
            "action-navigation" => "detailler-image",
            "id_image" => $image->getId_image(),
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefDetailler;

}



function assemblerHrefSupprimer($page, $image){

    $hrefSupprimer =  $page->pageSuppressionImageURL. "?" .
        http_build_query(
        [
            "id_image" => $image->getId_image(),
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefSupprimer;

}


function afficherListeImageAction($page = null){
    $accesseurImage = new AcesseurImage();
    $page->listeImage = $accesseurImage->recupererListeImage();

    if(!is_object($page)) return;

   

}
afficherPage($page);
