<?php
require_once("../accesseur/AccesseurMembre.php");
require_once("../modele/Membre.php");

function ajouterMembre($membre) {

    $accesseurMembre = new AccesseurMembre();
    $accesseurMembre->ajouterMembre($membre);
    //Ajouter la membre avec $membre dans la BD.

    //Si erreur
    if($erreur) return false;

    return true;

}

if($_GET["navigation-retour-url"] ?? false &&
   $_GET["navigation-retour-titre"] ?? false){

    $page->navigationRetourURL = $_GET["navigation-retour-url"];
    $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

}else if($_POST["navigation-retour-url"] ?? false &&
         $_POST["navigation-retour-titre"] ?? false){

    $page->navigationRetourURL = $_POST["navigation-retour-url"];
    $page->navigationRetourTitre = $_POST["navigation-retour-titre"];

}

$page->membre = new Membre((object) $_POST);

if($_POST["action-aller-seconde-etape"] ?? false){

    if($page->membre->isValide(Membre::NOM_UTILISATEUR)){

        $page->isSecondeEtape = true;
        $page->isPremiereEtape = false;
        $page->isTroisiemeEtape = false;

        $page->titreEtape = "Seconde étape";

    }else{

        $page->messageAction =
            "Erreur à la première étape";

        $page->isSecondeEtape = false;
        $page->isPremiereEtape = true;
        $page->isTroisiemeEtape = false;

        $page->titreEtape = "Premiere étape";

    }

}else if($_POST["action-aller-troisieme-etape"] ?? false){

    if($page->membre->isValide(Membre::COURRIEL))
    {

        $page->isSecondeEtape = false;
        $page->isPremiereEtape = false;
        $page->isTroisiemeEtape = true;

        $page->titreEtape = "Troisième étape";

    }else{

        $page->messageAction =
            "Erreur à la seconde étape";

        $page->isSecondeEtape = true;
        $page->isPremiereEtape = false;
        $page->isTroisiemeEtape = false;

        $page->titreEtape = "Seconde etape";

    }
}else if($_POST["action-inscrire"] ?? false){

    //if($page->membre->isValide(Membre::MOT_DE_PASSE)&& $_POST["mot_de_passe"]==$_POST["confirmerMotDePasse"]){

        if(ajouterMembre(new Membre((object)$_POST))){

            $page->isSecondeEtape = false;
            $page->isPremiereEtape = false;
            $page->isTroisiemeEtape = false;

            $page->messageNavigationRetour =
                        "Inscription réussie";

            $page->isNavigationRetour = true;

        }

        else{
            $page->messageAction =
                "Erreur de l'inscription";
                $page->isSecondeEtape = false;
                $page->isPremiereEtape = false;
                $page->isTroisiemeEtape = true;
        }


    //}else{

        //$page->messageAction =
            //"Erreur à la troisième étape";

        //$page->isSecondeEtape = false;
        //$page->isPremiereEtape = false;
        //$page->isTroisiemeEtape = true;

        //$page->titreEtape = "Troisième étape";

    //}

}else if($_POST["action-retourner-premiere-etape"] ?? false){

    $page->isSecondeEtape = false;
    $page->isPremiereEtape = true;
    $page->isTroisiemeEtape = false;

    $page->titreEtape = "Première étape";

}else if($_POST["action-retourner-seconde-etape"] ?? false){

    $page->isSecondeEtape = true;
    $page->isPremiereEtape = false;
    $page->isTroisiemeEtape = false;

    $page->titreEtape = "Seconde étape";

}

else if($_POST["action-retourner-troisieme-etape"] ?? false){

    $page->isSecondeEtape = false;
    $page->isPremiereEtape = false;
    $page->isTroisiemeEtape = true;

    $page->titreEtape = "Troisième etape";

}



afficherPage($page);
?>
