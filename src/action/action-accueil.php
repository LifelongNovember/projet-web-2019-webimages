<?php
require_once "../accesseur/AccesseurImage.php";

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if(isset($_GET["local"])){
  $locale=$_GET["local"];
}
else if(isset($_SESSION["local"])){
  $locale=$_SESSION["local"];
}
else{
  $locale="en_EN";
}

putenv("LANG=" . $locale);
setlocale(LC_ALL, $locale);
$domain = "messages";

bindtextdomain($domain, "..\local");

bind_textdomain_codeset($domain, 'UTF_8');

textdomain($domain);

$accesseurImage = new AcesseurImage();
if (!empty($_POST['recherche']))
{
  $recherche = filter_var($_POST['recherche'], FILTER_SANITIZE_STRING);
  //var_dump($recherche);
  $page->listeImage = $accesseurImage->rechercherSuggestionsImage($recherche);
}
else
  $page->listeImage = $accesseurImage->recupererListeImage();

//var_dump($page->listeImage);
afficherPage($page);
?>
