<?php

require_once "../accesseur/AccesseurImage.php";

$image= (object)
[
"titreImage"=>"titre image",
"image"=>"https://placehold.it/600x370&text=Look at me!",
"prixImage"=>"10000"
];



  if($_GET["navigation-retour-url"] ?? false &&
     $_GET["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_GET["navigation-retour-url"];
      $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

  }else if($_POST["navigation-retour-url"] ?? false &&
           $_POST["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_POST["navigation-retour-url"];
      $page->navigationRetourTitre = $_POST["navigation-retour-titre"];

  }

  if($_GET["id_utilisateur"] ?? false){

      if(!$page->image = recupererImage(new Image((object) $_GET))){

          $page->messageNavigationRetour = _("Utilisateur non trouvée").
          $page->isNavigationRetour = true;

      }

  }

  if(!$page->isNavigationRetour){

    $page->image = $image;
/*
    if(ajouterImage($page->image)){

          $page->messageNavigationRetour =
              "mise en vente réussie";

          $page->isNavigationRetour = true;

      }
      else{

          $page->messageAction =
              "Erreur lors de la vente";

          $page->titre = "Vendre une image";
      }*/
  }

  afficherPage($page);
 ?>
