<?php
include_once("../action/action-utilisateur-est-connecter.php");
require_once("../accesseur/AccesseurPayment.php");
require_once("../accesseur/AccesseurImage.php");
require_once("../modele/Payment.php");
require_once("../modele/Image.php");
$page->hrefgestionUtilisateur = $page->pageGestionUtilisateurURL . "?" .
  http_build_query(
    [
        "action-navigation" => "liste-utilisateur",
        "navigation-retour-url" => $page->URL,
        "navigation-retour-titre" => $page->titrePage
    ]);

        //URL et paramètre de la page de destination
        //pour gerer les image
$page->hrefgestionImage = $page->pageGestionImageURL . "?" .
    http_build_query(
    [
        "action-navigation" => "liste-image",
        "navigation-retour-url" => $page->URL,
        "navigation-retour-titre" => $page->titrePage
    ]);

$accesseurPayment = new AcesseurPayment();
$accesseurImage = new AcesseurImage();
$page->listeTransaction = $accesseurPayment->recupererListePayment();

foreach($page->listeTransaction as $transaction){
#  print_r($transaction->getid_Image());
  $transaction->montant = $accesseurImage->recupererImage($transaction->getid_Image())->getPrix();
}
afficherPage($page);
