<?php
require_once('../accesseur/AccesseurImage.php');

  $recherche = filter_var($_GET['recherche'], FILTER_SANITIZE_STRING);
	$accesseurImage = new AccesseurImage();
	$suggestions = $accesseurImage->rechercherSuggestionsImage($recherche);

  afficherPage($page);
?>
