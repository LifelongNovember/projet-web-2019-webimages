<?php
include_once "../accesseur/AccesseurImage.php";
$accesseurImage = new AcesseurImage();
$recherche = filter_var($_GET['recherche'], FILTER_SANITIZE_STRING);
$page->listeImage = $accesseurImage->rechercherSuggestionsImage($recherche);

afficherPage($page);
