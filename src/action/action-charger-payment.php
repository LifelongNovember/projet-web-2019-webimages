<?php
require_once("../lib/vendor/stripe/stripe-php/init.php");
require_once("../accesseur/AccesseurImage.php");
require_once("../accesseur/AccesseurPayment.php");
require_once("../modele/Payment.php");
\Stripe\Stripe::setApiKey("sk_test_JF3wgN5FcPJYVuktsaUc34vn");
$image = new Image((object) $_POST);
$acesseurImage = new AcesseurImage();
$image = $acesseurImage->recupererImage($image->getId_image());
$token = $_POST['stripeToken'];
$charge = \Stripe\Charge::create([
    'amount' => $image->getPrix()*100,
    'currency' => 'cad',
    'description' => $image->getNom(),
    'source' => $token,
]);

$accesseurPayment = new AcesseurPayment();
$objPayment = (object) [
  Payment::ID_IMAGE => $image->getId_image(),
  Payment::ID_MEMBRE => $_SESSION['profil']->id_membre
];
$payment = new Payment($objPayment);
$accesseurPayment->ajouterPayment($payment);
header("Location: ../vue/index.php");
