<?php
  include_once "../accesseur/AccesseurCommentaire.php";
  include_once "../accesseur/AccesseurImage.php";
  $accesseurImage = new AcesseurImage();
  $accesseurCommentaire= new AccesseurCommentaire();
  $imageobj = new Image((object) $_GET);
  $page->image = $accesseurImage->recupererImage($imageobj->getId_image());

  if(isset($_POST["commentaire"])){
    $commentaireFormulaire=  new Commentaire((object)
        [
            "id_commentaire" =>null,
            "contenu" =>$_POST['commentaire']
        ]);
    $accesseurCommentaire->ajouterEntiteAffaire($commentaireFormulaire,$page->image->getId_image());
  }
  $listeCommentaire =   $accesseurCommentaire->recupererListeCommentaireParImage($imageobj->getId_image());

  $page->listeCommentaire = $listeCommentaire;


  afficherPage($page);
?>
