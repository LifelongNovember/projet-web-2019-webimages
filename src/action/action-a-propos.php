<?php

if($_GET["navigation-retour-url"] ?? false &&
     $_GET["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_GET["navigation-retour-url"];
      $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

} else if($_POST["navigation-retour-url"] ?? false &&
           $_POST["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_POST["navigation-retour-url"];
      $page->navigationRetourTitre = $_POST["navigation-retour-titre"];
}

afficherPage($page);
