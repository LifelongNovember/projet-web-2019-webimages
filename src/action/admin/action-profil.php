<?php
include_once("action/action-utilisateur-est-connecter.php");
include_once("action-utilisateur-est-admin.php");

// require_once("Personne.php");
require_once("../modele/Membre.php");
require_once("../accesseur/AccesseurMembre.php");

function recupererPersonne() {
  // TODO : Modele
  if(!empty($_GET['id_membre'])){
    $accesseurMembre = new AccesseurMembre();
    $personne = $accesseurMembre->recupererMembre(new Membre($obj = (object)[Membre::ID_MEMBRE => $_GET['id_membre']]));

    $personne->galerie =
    [
      "1.png",
      "2.png",
      "3.png",
      "4.png",
      "5.png",
    ];

    return $personne;
  }
}

function supprimerPersonne($personneRecherche) {

    //if(!$personneRecherche->isValide()) return false;
      $accesseurMembre = new AccesseurMembre();
    //Supprimer la personne avec $personneRecherche->id_personne dans la BD.

    return $accesseurMembre->supprimerMembre(recupererPersonne());

}

function modifierPersonne($personne) {

    //if(!$personne->isValide()) return false;
      $accesseurMembre = new AccesseurMembre();


    return $accesseurMembre->modifierMembre($personne);;

}

if($_GET["navigation-retour-url"] ?? false &&
   $_GET["navigation-retour-titre"] ?? false){

    $page->navigationRetourURL = $_GET["navigation-retour-url"];
    $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

}else if($_POST["navigation-retour-url"] ?? false &&
         $_POST["navigation-retour-titre"] ?? false){

    $page->navigationRetourURL = $_POST["navigation-retour-url"];
    $page->navigationRetourTitre = $_POST["navigation-retour-titre"];

}

if(!$page->isNavigationRetour){

    switch ($_GET["action-navigation"] ?? "") {

        case "detailler-utilisateur":
            $page->titre = _("Les détails d'une personne");
            break;

        case "ajouter-utilisateur":
            $page->isAjouterPersonne = true;
            $page->titre = _("Ajouter une personne");
            break;

        case "modifier-utilisateur":
            $page->isModifierPersonne = true;
            $page->titre = _("Modifier une personne");
            break;

        case "supprimer-utilisateur":
            $page->isSupprimerPersonne = true;
            $page->titre = _("Supprimer une personne");
            break;
          }
            //Construire l'objet $page->personne
            //avec les données du formulaire.
       //     $page->personne = new Personne((object) $_POST);

            if($_POST["action-supprimer-personne"] ?? false){
                if(supprimerPersonne($page->personne)){
                  print_r("sprimer");
                    $page->messageNavigationRetour =
                        _("Suppression de personne réussie");

                    $page->isNavigationRetour = true;

                }
                else{
                    $page->messageAction =
                        _("Erreur de suppression de personne");

                    $page->isSupprimerPersonne = true;

                    $page->titre = _("Supprimer une personne");

                }
            }else if($_POST["action-modifier-personne"] ?? false){
              $personneModifier = (object)[
                Membre::ID_MEMBRE => $_GET[Membre::ID_MEMBRE],
                Membre::NOM_UTILISATEUR => $_POST[Membre::NOM_UTILISATEUR],
                Membre::COURRIEL => $_POST[Membre::COURRIEL],
                Membre::MOT_DE_PASSE => $_POST[Membre::MOT_DE_PASSE]
              ];
                if(modifierPersonne(new Membre($personneModifier))){

                    $page->messageNavigationRetour =
                        _("Modification de personne réussie");

                    $page->isNavigationRetour = true;

                }
                else{

                    $page->messageAction =
                        _("Erreur de modification de personne");

                    $page->isModifierPersonne = true;

                    $page->titre = _("Ajouter une personne");

                }

            }

    $page->isformulaireEditable = $page->isAjouterPersonne ||
                                  $page->isModifierPersonne ;
}

afficherPage($page);
