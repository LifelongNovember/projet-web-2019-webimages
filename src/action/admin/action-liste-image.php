<?php
include_once("action-utilisateur-est-admin.php");

require_once "../accesseur/AccesseurImage.php";
if($_GET["navigation-retour-url"] ?? false &&
     $_GET["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_GET["navigation-retour-url"];
      $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

  }else if($_POST["navigation-retour-url"] ?? false &&
           $_POST["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_POST["navigation-retour-url"];
      $page->navigationRetourTitre = $_POST["navigation-retour-titre"];

  }

function assemblerHrefAjouter($page){

    $hrefAjouter = $page->pageAjoutImageURL."?" .
        http_build_query(
        [
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefAjouter;

}

function assemblerHrefDetailler($page, $image){

    $hrefDetailler = $page->pageGestionImageURL . "?" .
        http_build_query(
        [
            "action-navigation" => "detailler-image",
            "id_image" => $image->getId_image(),
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefDetailler;

}



function assemblerHrefSupprimer($page, $image){

    $hrefSupprimer =  $page->pageSuppressionImageURL. "?" .
        http_build_query(
        [
            "id_image" => $image->getId_image(),
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefSupprimer;

}


function afficherListeImageAction($page = null){
    $accesseurImage = new AcesseurImage();
    $page->listeImage = $accesseurImage->recupererListeImage();

    if(!is_object($page)) return;

    if($page->listeImage){

        //URL et paramètre de la page de destination
        //pour ajouter une image
        $hrefAjouter = assemblerHrefAjouter($page);

    ?>

<a href="<?= $hrefAjouter; ?>">Ajouter une Image</a>

<div class="listeImage">

    <?php

    foreach ($page->listeImage as $image){


        $UrlItem = $image->getImage();
        $altItem = $image->getDescription();
        $titreItem = $image->getNom();

        //URL et paramètre de la page de destination
        //pour détailler une image
        $hrefDetailler = assemblerHrefDetailler($page, $image);


        //URL et paramètre de la page de destination
        //pour supprimer une personne
        $hrefSupprimer = assemblerHrefSupprimer($page, $image);

    ?>

    <div class="image">
        <a href="<?= $hrefDetailler; ?>"><img src="<?= $UrlItem; ?>" alt="<?= $altItem; ?>"/><?=$titreItem?></a>
        <a href="<?= $hrefSupprimer; ?>" > <button type='button'><?php echo _("Supprimer") ?></button></a>
    </div>

    <?php
    }
    ?>

</div>

    <?php
    }

}
afficherPage($page);
