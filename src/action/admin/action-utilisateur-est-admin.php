<?php
/**
 * Created by PhpStorm.
 * User: Eloi
 * Date: 13/03/2019
 * Time: 14:07
 */
require_once("../accesseur/AccesseurMembre.php");
function retourAcceuil(){
    header("location: connexion.php");
    die();
}

if(!$accesseurMembre->recupererMembreParNom($utilisateur)->getEstAdmin()) retourAcceuil();