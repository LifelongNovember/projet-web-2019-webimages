<?php
include_once("action-utilisateur-est-admin.php");

require_once "../accesseur/AccesseurMembre.php";
if($_GET["navigation-retour-url"] ?? false &&
     $_GET["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_GET["navigation-retour-url"];
      $page->navigationRetourTitre = $_GET["navigation-retour-titre"];

  }else if($_POST["navigation-retour-url"] ?? false &&
           $_POST["navigation-retour-titre"] ?? false){

      $page->navigationRetourURL = $_POST["navigation-retour-url"];
      $page->navigationRetourTitre = $_POST["navigation-retour-titre"];

  }

function assemblerHrefAjouter($page){

    $hrefAjouter = $page->pageGestionPersonneURL . "?" .
        http_build_query(
        [
            "action-navigation" => "ajouter-utilisateur",
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefAjouter;

}

function assemblerHrefDetailler($page, $personne){

    $hrefDetailler = $page->pageGestionPersonneURL . "?" .
        http_build_query(
        [
            "action-navigation" => "detailler-utilisateur",
            "id_membre" => $personne->getId_membre(),
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefDetailler;

}

function assemblerHrefModifier($page, $personne){

    $hrefModifier = $page->pageGestionPersonneURL . "?" .
        http_build_query(
        [
            "action-navigation" => "modifier-utilisateur",
            "id_membre" => $personne->getId_membre(),
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefModifier;

}

function assemblerHrefSupprimer($page, $personne){

    $hrefSupprimer = $page->pageGestionPersonneURL . "?" .
        http_build_query(
        [
            "action-navigation" => "supprimer-utilisateur",
            "id_membre" => $personne->getId_membre(),
            "navigation-retour-url" => $page->URL,
            "navigation-retour-titre" => $page->titrePage
        ]);

    return $hrefSupprimer;

}


function afficherListeUtilisateurAction($page = null){
$accesseurMembre = new AccesseurMembre();

$page->listeUtilisateur = $accesseurMembre->recupererListeMembre();

    if(!is_object($page)) return;

    if($page->listeUtilisateur){

        //URL et paramètre de la page de destination
        //pour ajouter une personne
        $hrefAjouter = assemblerHrefAjouter($page);

    ?>

<ul>

    <?php

    foreach ($page->listeUtilisateur as $utilisateur){

        //Le texte du lien les détails d'une personne
        $textItem = $utilisateur->getNom_utilisateur();

        //URL et paramètre de la page de destination
        //pour détailler une personne
        $hrefDetailler = assemblerHrefDetailler($page, $utilisateur);

        //URL et paramètre de la page de destination
        //pour modifier une personne
        $hrefModifier = assemblerHrefModifier($page, $utilisateur);

        //URL et paramètre de la page de destination
        //pour supprimer une personne
        $hrefSupprimer = assemblerHrefSupprimer($page, $utilisateur);

    ?>

    <li>
        <a href="<?= $hrefDetailler; ?>"><?= $textItem; ?></a>
        <a href="<?= $hrefModifier; ?>"><?php echo _("Modifier") ?></a>
        <a href="<?= $hrefSupprimer; ?>"><?php echo _("Supprimer") ?></a>
    </li>

    <?php
    }
    ?>

</ul>

    <?php
    }

}
afficherPage($page);
