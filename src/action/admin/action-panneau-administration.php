<?php
echo(__DIR__);
require_once("../action-utilisateur-est-connecter.php");
require_once("action-utilisateur-est-admin.php");

function afficherPanneauAdministrationAction($page = null){

    /*
    Attributs de $page utilisés:

    $page->panneauAdministration
    $page->pageGestionUtilisateurURL
    $page->pageGestionImageURL
    $page->pageGestionCommentaireURL
    $page->URL
    $page->titre
    */

    if(!is_object($page)) return;



        //URL et paramètre de la page de destination
        //pour gerer les utilisateur
        $hrefgestionUtilisateur = $page->pageGestionUtilisateurURL . "?" .
            http_build_query(
            [
                "action-navigation" => "liste-utilisateur",
                "navigation-retour-url" => $page->URL,
                "navigation-retour-titre" => $page->titrePage
            ]);

        //URL et paramètre de la page de destination
        //pour gerer les image
        $hrefgestionImage = $page->pageGestionImageURL . "?" .
            http_build_query(
            [
                "action-navigation" => "liste-image",
                "navigation-retour-url" => $page->URL,
                "navigation-retour-titre" => $page->titrePage
            ]);


    ?>



<a href="<?= $hrefgestionUtilisateur; ?>"><?php echo _("Gerer les utilisateurs") ?></a>

<a href="<?= $hrefgestionImage; ?>"><?php echo _("Gerer les images") ?></a>


    <?php

}


afficherPage($page);
