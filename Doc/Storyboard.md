# storyboard

1. ## achat image
 cyrillehamel
 ![achat image](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/Doc/storyboard/StoryBoard%20achat%20image.png)
2. ## acceuil
 aden-iutsd
  ![acceuil](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/Doc/storyboard/Storyboard%20accueil.png)
3. ## ajout d'un commentaire
 NorwellFram
  ![ajout d'un commentaire](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/Doc/storyboard/Storyboard%20ajout%20d'un%20commentaire%20sur%20une%20image.png)
4. ## mise en vente
 NorwellFram
  ![mise en vente](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/Doc/storyboard/Storyboard%20mise%20en%20vente%20d%20une%20image.png)
5. ## inscription
 Bassirou1
  ![inscription](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/Doc/storyboard/Storyboard%20pour%20la%20page%20d'inscription.png)
6. ## modification des donnee membre
 gui45
  ![modification des donnee membre](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/Doc/storyboard/storyboard%20modification%20de%20donnees%20personnels.png)
