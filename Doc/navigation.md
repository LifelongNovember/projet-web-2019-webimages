# plan des url

|       page           |      url   brut           |      url   reecrit      |
|:--------------------:|:-------------------------:|:-----------------------:|
| index                |/index.php                 | /                       |
| a propos             |/a-propos.php              | /a-propos               |
| connexion            |/connexion.php             | /connexion              |
| inscription          |/inscription.php           | /inscription            |
|vendre une image      |/vente-une-image.php       | /vente                  |
| detail image         |/detail-image.php          | /detail                 |
|liste des utilisateur |/liste-utilisateur.php     | /liste/utilisateurs     |
|liste des images      |/liste-image.php           | /liste/images           |
|Profil utilisateur    |/profil.php                | /profil                 |
|panneau administration|/panneau-administration.php| /administration         |
|achat d'une image     |/achat-une-image.php       | /achat                  |
|recherche d'une image |/recherche-une-image.php   | /recherche              |




