# Feuille de temps Bassirou Haidara
|numero iteration|numero semaine|date du jour|nb heures|numero issue|lien issue|commentaires|lien ver document|
|:--------------:|:------------:|:----------:|:-------:|:----------:|:--------:|:----------:|:---------------:|
| 1              |1             |24/01/2019  |3        |X           | X        |  Rencontre, partage des idées de notre site web         |[documentation](https://github.com/cegepmatane/projet-web-2019-webimages/tree/master/Doc)|
| 1              |2             |28/01/2019  |3        |1           |[issue 1](https://github.com/cegepmatane/projet-web-2019-webimages/issues/1)        | Début et finalisation des story-boards et diagramme de navigation         |X|
| 1              |2             |30/01/2019  |2        |4           |[issue 4](https://github.com/cegepmatane/projet-web-2019-webimages/issues/4)        |   Début de la vue du site        |X|
| 1              |2             |31/01/2019  |2        |4           |[issue 4](https://github.com/cegepmatane/projet-web-2019-webimages/issues/4)        |   Création de la vue page d'acceuil avec css       |X|
| 1              |3             |04/02/2019  |3        |6           |[issue 6](https://github.com/cegepmatane/projet-web-2019-webimages/issues/6)        | Correction page inscription et debut des controleur          |X|
| 1              |3             |06/02/2019  |2        |7          |[issue 7](https://github.com/cegepmatane/projet-web-2019-webimages/issues/7)      |    creation de la page connexion       |X|
| 1              |3             |07/02/2019  |3       |7          |[issue 11](https://github.com/cegepmatane/projet-web-2019-webimages/issues/11)      | Création du constructeur du modèle membre         |X|
| 1              |3             |07/02/2019  |3       |11          |[issue 11](https://github.com/cegepmatane/projet-web-2019-webimages/issues/11)      | Fin du modèle membre avec validation et sécurisation des champs         |X|
| 1              |3             |11/02/2019  |4      |16          |[issue 16](https://github.com/cegepmatane/projet-web-2019-webimages/issues/16)      | Début et finalisation du DAO membre         |X|
| 1              |4             |13/02/2019  |3      |6          |[issue 6](https://github.com/cegepmatane/projet-web-2019-webimages/issues/6)      | Tranformation de la page d'inscription en multipage         |X|
| 1              |4             |14/02/2019  |3      |16 , 11          |[issue 16](https://github.com/cegepmatane/projet-web-2019-webimages/issues/16)      | Changement du DAOMembre et du Modele en fonction du multipage         |X|
| 1              |5             |16/02/2019  |2      |16 , 11 6         |[issue 6](https://github.com/cegepmatane/projet-web-2019-webimages/issues/6)      | Correction des bug dans le formulaire multipage         |X|
| 1              |5             |16/02/2019  |3      |16 , 11 7        |[issue 7](https://github.com/cegepmatane/projet-web-2019-webimages/issues/7)      | Correction des bug dans la page connexion         |X|
| 1              |6-7             |14/03/2019  |5     |29       |[issue 29](https://github.com/cegepmatane/projet-web-2019-webimages/issues/29)      | ajout de l'Action rechercher une image         |X|
