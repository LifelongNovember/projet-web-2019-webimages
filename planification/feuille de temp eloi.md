# feuille de temp Eloi Montanaro

|numero iteration|numero semaine|date du jour|nb heures|numero issue|commentaires|lien ver document|
|:--------------:|:------------:|:----------:|:-------:|:----------:|:----------:|:---------------:|
| 1              |1             |24/01/2019  |3        |[1](https://github.com/cegepmatane/projet-web-2019-webimages/issues/1)|  X         |[documentation](https://github.com/cegepmatane/projet-web-2019-webimages/tree/master/Doc) |
| 1				 |1				|25/01/2019  |3        |[1](https://github.com/cegepmatane/projet-web-2019-webimages/issues/1)|       x    |[documentation](https://github.com/cegepmatane/projet-web-2019-webimages/tree/master/Doc) |
| 1				 |2				|28/01/2019  |2        |[1](https://github.com/cegepmatane/projet-web-2019-webimages/issues/1)|       x    |[documentation](https://github.com/cegepmatane/projet-web-2019-webimages/tree/master/Doc) |
| 1				 |2				|30/01/2019  |3        |[5](https://github.com/cegepmatane/projet-web-2019-webimages/issues/5)|vente img   |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/vente-une-image.php) |
| 1				 |2				|31/01/2019  |3        |[5](https://github.com/cegepmatane/projet-web-2019-webimages/issues/5)|achat img   |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/achat-une-image.php) |
| 1				 |3				|04/02/2019  |2        |[9](https://github.com/cegepmatane/projet-web-2019-webimages/issues/9)|recherche   |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/recherche-une-image.php) |
| 1				 |3				|06/02/2019  |3        |[13](https://github.com/cegepmatane/projet-web-2019-webimages/issues/13)|BD          |[Base de donnee](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/script_creation_BD.sql) |
| 1				 |3				|07/02/2019  |3        |[17](https://github.com/cegepmatane/projet-web-2019-webimages/issues/17)|traduction  |[Langue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/Langue/en.php) |
| 1				 |4				|11/02/2019  |2        |[18](https://github.com/cegepmatane/projet-web-2019-webimages/issues/18)|traduction  |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/index.php) |
| 1				 |4				|13/02/2019  |3        |[18](https://github.com/cegepmatane/projet-web-2019-webimages/issues/18)|traduction  |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/index.php) |
| 1				 |4				|14/02/2019  |3        |[18](https://github.com/cegepmatane/projet-web-2019-webimages/issues/18)|traduction  |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/index.php) |
| 1				 |5				|18/02/2019  |2        |[17](https://github.com/cegepmatane/projet-web-2019-webimages/issues/17)| vente img |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/index.php) |
| 1				 |5				|21/02/2019  |3        |[18](https://github.com/cegepmatane/projet-web-2019-webimages/issues/18)|traduction  |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/index.php) |
| 1				 |7				|04/03/2019  |3        |[18](https://github.com/cegepmatane/projet-web-2019-webimages/issues/18)|traduction  |[vue](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/index.php) |
| 1				 |7				|07/03/2019  |2        |[22](https://github.com/cegepmatane/projet-web-2019-webimages/issues/22)|refactor |[projet](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage) |
| 1				 |8				|13/03/2019  |2        |[11](https://github.com/cegepmatane/projet-web-2019-webimages/issues/11)|ajout est_admin |[modele](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/modele) |
