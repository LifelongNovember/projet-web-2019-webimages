﻿# Feuille de temps A. DENIS

No. Itération | No. Semaine | Date du jour | Nb. Heures | Issue | Fichier | Commentaire
-|-|-|-|-|-|-
1 | 2 | 13/02/2019 | 5 | [#5](https://github.com/cegepmatane/projet-web-2019-webimages/issues/5) Profil  | [[profil.php](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/profil.php)] | Affichage informations utilisateur | 
1 | 2 | 13/02/2019 | 3 | [#2](https://github.com/cegepmatane/projet-web-2019-webimages/issues/2) footer publique | [[header-footer.css](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/decoration/header-footer.css)] | Style header/footer
1 | 2/3 | 13/02/2019 | 5 | [#17](https://github.com/cegepmatane/projet-web-2019-webimages/issues/17) vue vente image | [[vente-une-image.php](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/vue/vente-une-image.php)] | Upload d'images en drag&drop
1 | 3 | 13/02/2019 | 1 | [#6](https://github.com/cegepmatane/projet-web-2019-webimages/issues/6) Inscription membre | [[inscription.php](https://github.com/cegepmatane/projet-web-2019-webimages/blob/master/p2pimage/modele/Inscription.php)] | Modèle / accesseur inscription
1 | 3/4 | 13/02/2019 | 3 | (Administration système - Pas d'issue) | / | Mise en place des services mysql / phpmyadmin / fail2ban
